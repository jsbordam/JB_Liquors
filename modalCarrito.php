<?php 
    require "logica/Carrito.php";
    require "logica/Producto.php";
    require "persistencia/Conexion.php";
       
    
    //Primero busco el carrito de productos que tiene registrado ese idPedido
    $carrito = new Carrito ($_GET["idPedido"]);
    
    //Para el numero de productos dentro del carrito
    $carrito -> sumarProductos();
    
    //Me trae un array de todos los registros asociados a ese pedido
    $arregloCarrito = $carrito -> consultarTodos(); 
    
?>

<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel"><font face="Algerian" size="5" color="black">PRODUCTOS EN TU CARRITO</font></h5>
	<button type="button" class="close" data-dismiss="modal"
		aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>

<div id="actualizar" class="modal-body">
	<table class="table table-hover">
    	<?php 
            $valorTotal=0;
        	foreach ($arregloCarrito as $registroActual)
        	{      	    
        	    //Busco los datos del producto de cada registro:
        	    $producto = new Producto ($registroActual->getIdProducto());
        	    $producto -> consultar();
        	    
        	    $valorCantidad = $producto->getValor() * $registroActual->getCantidad_und();
        	    $valorTotal += $valorCantidad;
        	    
        	    //Muestro los datos del producto que necesito:
        	    echo "<tr>";
        	        echo "<td><img src='" . $producto->getFoto() . "' width='100px' height='100px'></td>";  
        	        echo "<td>"; 
        	        echo "<font face='Arial Black' size='3' color='white'>" . $producto->getNombre() . " " . $producto->getDescripcion() . "</font>";
        	           echo "<h5><font face='Arial Black' size='2' color='white'>-> $" . number_format($producto->getValor(), ...array(0, ',', '.')) . "</font></h5>";      	           
        	           echo "<h5><font face='Arial' size='3' color='white'>Cantidad x " . $registroActual->getCantidad_und() . "</font></h5>";
        	           echo "<b><h5><font face='Arial' size='3' color='white'>$" . number_format($valorCantidad, ...array(0, ',', '.')) . "</font></h5></b>";
        	        echo "</td>";       	        
        	        echo "<td><a href='#'><font size='5' color='red'><i id='eliminarProducto" . $registroActual->getIdProducto() . "' class='far fa-trash-alt' data-toggle='tooltip' data-placement='bottom' title='Eliminar'></i></font></a></td>";         	       
        	   echo "</tr>";  
        	}       	
    	?>
	</table>
	<?php  
	   //Cuando el carrito esta vacio
        if ($carrito->getCantidad_und() == "")
        {
            echo "<br><br><br><br><br><br><br>";
            echo "<h2 class='text-center'>Tu carrito de compras esta vacio!</h2>";         
        }
        else 
        {
    ?>
	<div class='row'>
		<div class='col-md-9'>
            <h4><font face='Arial' size='4' color='black'>Sub -Total</font></h4>           
  		</div>
  		<div class='col-md-3'>
            <h5><font face='Arial Black' size='4' color='black'>$<?php echo number_format($valorTotal, ...array(0, ',', '.'))?></font></h5>       
  		</div>	
  	</div>
  	<div class='row'>
		<div class='col-md-9'>
            <h4><font face='Arial' size='4' color='black'>Total</font></h4>           
  		</div>
  		<div class='col-md-3'>
            <h5><font face='Arial Black' size='4' color='black'>$<?php echo number_format($valorTotal, ...array(0, ',', '.'))?></font></h5>       
  		</div>	
  	</div>
  	<?php 
        }
  	?>
</div>

<div class="modal-footer">
	<a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/factura.php")?>&m&idPedido=<?php echo $_GET["idPedido"]?>" class='btn float-right modal_btn'>
		VER FACTURA
	</a>
	<button type="button" class="btn float-right modal_btn2" data-dismiss="modal">CERRAR</button>
</div>



<!--Este es el JQuery para eliminar un producto del carrito: -->
<script>
    $(document).ready(function()
    {
        <?php 
            foreach ($arregloCarrito as $registroActual)
            {
                echo "$(\"#eliminarProducto" . $registroActual->getIdProducto() . "\").click(function()\n";
                echo "{\n";
                //Actualiza numero del carrito:
                echo "    url2 = \"indexAjax.php?pid=" . base64_encode("presentacion/cliente/actualizarCarritoAjax.php") . "&idPedido=" . $registroActual->getIdPedido() . "\"\n";
                echo "    $(\"#actualizarC\").load(url2);\n";
                //Definimos la ruta en donde se recargara esta capa y la encriptamos respectivamente:
                echo "    url = \"indexAjax.php?pid=" . base64_encode("presentacion/cliente/eliminarProductoAjax.php") . "&idProducto=" . $registroActual -> getIdProducto() . "&idPedido=" . $registroActual -> getIdPedido() . "\"\n";
                echo "    $(\"#actualizar\").load(url);\n";
                //Actualiza numero del carrito nuevamente:
                echo "    url2 = \"indexAjax.php?pid=" . base64_encode("presentacion/cliente/actualizarCarritoAjax.php") . "&idPedido=" . $registroActual->getIdPedido() . "\"\n";
                echo "    $(\"#actualizarC\").load(url2);\n";
                echo "});\n\n";
            }                  
        ?> 
    });
</script>