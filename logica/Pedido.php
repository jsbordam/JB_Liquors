<?php
    //Para importar las clases que necesito usar:
    require "persistencia/PedidoDAO.php";
    
    class Pedido
    {
        private $idPedido;
        private $fecha_hora;
        private $valor_total;
        private $estado;
        private $idUsuario;
        
        private $conexion;
        private $pedidoDAO;
        
        
        //Constructor:
        
        function Pedido ($pIdPedido="", $pFecha_hora="", $pValor_total="", $pEstado="", $pIdUsuario="")
        {
            $this -> idPedido = $pIdPedido;
            $this -> fecha_hora = $pFecha_hora;
            $this -> valor_total = $pValor_total;
            $this -> estado = $pEstado;
            $this -> idUsuario = $pIdUsuario;
            $this -> conexion = new Conexion();
            $this -> pedidoDAO = new PedidoDAO ($pIdPedido, $pFecha_hora, $pValor_total, $pEstado, $pIdUsuario);
        }
        
        
        //Metodos GET:
        
        public function getIdPedido()
        {
            return $this->idPedido;
        }
        
        public function getFecha_hora()
        {
            return $this->fecha_hora;
        }
        
        public function getValor_total ()
        {
            return $this->valor_total;
        }
            
        public function getEstado ()
        {
            return $this->estado;
        }
            
        public function getIdUsuario ()
        {
            return $this->idUsuario;
        }
        
        
        
        //Para recoger algunos datos del usuario para el LOG:
        

        public $ip;
        public $so;
        function datosLog()
        {  
            //Capturamos su Ip:
            $m = "";
            $externalContent = file_get_contents('http://checkip.dyndns.com/');
            preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
            
            $this->ip = $m[1];
            
            
            //Capturamos su Sistema Operativo (SO):
            $identificador = getenv("HTTP_USER_AGENT");
            
            
            function getPlatform($identificador)
            {
                $plataformas = array(
                    'Windows 10' => 'Windows NT 10.0+',
                    'Windows 8.1' => 'Windows NT 6.3+',
                    'Windows 8' => 'Windows NT 6.2+',
                    'Windows 7' => 'Windows NT 6.1+',
                    'Windows Vista' => 'Windows NT 6.0+',
                    'Windows XP' => 'Windows NT 5.1+',
                    'Windows 2003' => 'Windows NT 5.2+',
                    'Windows' => 'Windows otros',
                    'iPhone' => 'iPhone',
                    'iPad' => 'iPad',
                    'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
                    'Mac otros' => 'Macintosh',
                    'Android' => 'Android',
                    'BlackBerry' => 'BlackBerry',
                    'Linux' => 'Linux',
                );
                foreach ($plataformas as $plataforma=>$pattern)
                {
                    if (preg_match('/(?i)'.$pattern.'/', $identificador))
                        return $plataforma;
                }
                return 'Otras';
            }
            
            $this->so = getPlatform($identificador);
        }
        
                    
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear un pedido:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> pedidoDAO -> crear();
            $this -> conexion -> ejecutar($this -> pedidoDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Se genera reporte de LOG:
            $this -> datosLog();
            $this->consultar();
            
            $datos = "Codigo: " . $this -> idPedido .
                     "   Estado: SELECCIONANDO";
            
            $log = new Log ("", "Creacion de Pedido", $datos, date("Y/m/d"), date("G:i:s"), $this->ip, $this->so, $_SESSION["id"]);
            $log -> crear();
        }
        
        
        //Metodo para consultar un pedido que tenga estado 1 (SELECCIONADO):
        function consultar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> pedidoDAO -> consultar();
            $this -> conexion -> ejecutar($this -> pedidoDAO -> consultar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $resultado = $this -> conexion -> extraer();
            if ($this -> conexion -> numFilas() == 0)
            {
                //Quiere decir que no se encontro ningun resultado, 
                //entonces es el primer pedido:
                $this -> estado = 0;
            }
            else
            {
                $this -> idPedido = $resultado[0];
                $this -> estado = $resultado[1];
            }         
        }  
        
        //Metodo para consultar un pedido especifico:
        function consultarUno()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> pedidoDAO -> consultarUno();
            $this -> conexion -> ejecutar($this -> pedidoDAO -> consultarUno());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Si el pedido existe lo trae:
            if ($this -> conexion -> numFilas() == 1)
            {
                $resultado = $this -> conexion -> extraer();
                $this -> idPedido = $resultado[0];
                $this -> fecha_hora = $resultado[1];
                $this -> valor_total = $resultado[2];
                $this -> estado = $resultado[3];
                return true;
            } 
            else 
            {
                return false;
            }
        }
        
        
        //Metodo para consultar todos los pedidos de un usuario:
        function consultarTodos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> pedidoDAO -> consultarTodos();
            $this -> conexion -> ejecutar($this -> pedidoDAO -> consultarTodos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $pedidos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($pedidos, new Pedido ($resultado[0], $resultado[1], $resultado[2], $resultado[3]));
            }
            return $pedidos;
        }
        
        //Metodo para consultar todos los pedidos que existen:
        function consultarTodos2()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> pedidoDAO -> consultarTodos2();
            $this -> conexion -> ejecutar($this -> pedidoDAO -> consultarTodos2());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $pedidos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($pedidos, new Pedido ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]));
            }
            return $pedidos;
        }
        
        //Metodo para editar el estado de un pedido, cuando es solicitado:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> pedidoDAO -> editar();
            $this -> conexion -> ejecutar($this -> pedidoDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Se genera reporte de LOG:
            $this -> datosLog();
            $datos = "Codigo: " . $this -> idPedido .
                     "   Estado: SOLICITADO" .
                     "   Fecha - Hora: " . $this -> fecha_hora .
                     "   Valor Total: " . $this -> valor_total;
            
            $log = new Log ("", "Solicitud de Pedido", $datos, date("Y/m/d"), date("G:i:s"), $this->ip, $this->so, $_SESSION["id"]);
            $log -> crear();
        }
        
        //Metodo para eliminar un pedido:
        function eliminar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> pedidoDAO -> eliminar();
            $this -> conexion -> ejecutar($this -> pedidoDAO -> eliminar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Se genera reporte de LOG:
            $this -> datosLog();
            $datos = "Codigo: " . $this -> idPedido;
            
            $log = new Log ("", "Cancelacion de Pedido", $datos, date("Y/m/d"), date("G:i:s"), $this->ip, $this->so, $_SESSION["id"]);
            $log -> crear();
        }
    }
?>