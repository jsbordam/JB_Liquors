<?php
    //Para importar las clases que necesito usar:
    require "persistencia/UsuarioDAO.php";
    
    class Usuario 
    {
        private $idUsuario;
        private $nombre;
        private $apellido;
        private $tipo_docto;
        private $num_docto;
        private $correo;
        private $clave;
        private $estado;
        private $foto;
        private $idRol;
        
        private $conexion;
        private $usuarioDAO;
        
        
    
        //Constructor:
        
        function Usuario($pIdUsuario="", $pNombre="", $pApellido="", $pTipo_docto="", $pNum_docto="", $pCorreo="", $pClave="", $pEstado="", $pFoto="", $pIdRol="") 
        {
            $this -> idUsuario = $pIdUsuario;
            $this -> nombre = $pNombre;
            $this -> apellido = $pApellido;
            $this -> tipo_docto = $pTipo_docto;
            $this -> num_docto = $pNum_docto;
            $this -> correo = $pCorreo;
            $this -> clave = $pClave;
            $this -> estado = $pEstado;
            $this -> foto = $pFoto;
            $this -> idRol = $pIdRol;
            $this -> conexion = new Conexion();
            $this -> usuarioDAO = new UsuarioDAO($pIdUsuario, $pNombre, $pApellido, $pTipo_docto, $pNum_docto, $pCorreo, $pClave, $pEstado, $pFoto, $pIdRol);
        }
        
        
        //Metodos GET:
        
        public function getIdUsuario()
        {
            return $this->idUsuario;
        }
        
        public function getNombre()
        {
            return $this->nombre;
        }
        
        public function getApellido()
        {
            return $this->apellido;
        }
        
        public function getTipo_docto()
        {
            return $this->tipo_docto;
        }
        
        public function getNum_docto()
        {
            return $this->num_docto;
        }
        
        public function getCorreo()
        {
            return $this->correo;
        }
        
        public function getClave()
        {
            return $this->clave;
        }
        
        public function getEstado()
        {
            return $this->estado;
        }
        
        public function getFoto()
        {
            return $this->foto;
        }
        
        public function getIdRol()
        {
            return $this->idRol;
        }
        
        
        //Para recoger algunos datos del usuario para el LOG:
        
        public $datos;
        public $accion;
        public $ip;
        public $so;
        function datosUsuario()
        {
            //Primero se identifica a quien esta creando:
            if ($this -> idRol == 1)
            {
                $this->accion = "de Administrador";
            }
            else if ($this -> idRol == 2)
            {
                $this->accion = "de Cliente";
            }
            else
            {
                $this->accion = "de Proveedor";
            }
            
            //Luego se identifica el estado con el que fue creado:
            if ($this -> estado == 1)
            {
                $est = "Habilitado";
            }
            else 
            {
                $est = "Deshabilitado";
            }
            
            
            //Datos que tendra el LOG:
            
            $this->datos =  "Nombre: " . $this->nombre . " " . $this->apellido .                           
                            "   Numero de Documento: " . $this->num_docto .
                            "   Correo: " . $this->correo .
                            "   Estado: " . $est; 
            
            //Capturamos su Ip:
            $m = "";
            $externalContent = file_get_contents('http://checkip.dyndns.com/');
            preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
            
            $this->ip = $m[1];
            
            
            //Capturamos su Sistema Operativo (SO):
            $identificador = getenv("HTTP_USER_AGENT");
            
            
            function getPlatform($identificador) 
            {
                $plataformas = array(
                    'Windows 10' => 'Windows NT 10.0+',
                    'Windows 8.1' => 'Windows NT 6.3+',
                    'Windows 8' => 'Windows NT 6.2+',
                    'Windows 7' => 'Windows NT 6.1+',
                    'Windows Vista' => 'Windows NT 6.0+',
                    'Windows XP' => 'Windows NT 5.1+',
                    'Windows 2003' => 'Windows NT 5.2+',
                    'Windows' => 'Windows otros',
                    'iPhone' => 'iPhone',
                    'iPad' => 'iPad',
                    'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
                    'Mac otros' => 'Macintosh',
                    'Android' => 'Android',
                    'BlackBerry' => 'BlackBerry',
                    'Linux' => 'Linux',
                );
                foreach($plataformas as $plataforma=>$pattern){
                    if (preg_match('/(?i)'.$pattern.'/', $identificador))
                        return $plataforma;
                }
                return 'Otras';
            }
            
            $this->so = getPlatform($identificador);       
        }
         
        
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear un usuario:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> crear();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion                 
            
            //Se genera reporte de LOG:
            $this -> datosUsuario();
            
            $log = new Log ("", "Creacion " . $this->accion, $this->datos, date("Y/m/d"), date("G:i:s"), $this->ip, $this->so, $_SESSION["id"]);
            $log -> crear();
        }
        
        
        //Metodo para autenticar:
        function autenticar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> autenticar();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> autenticar());
            $this -> conexion -> cerrar(); //Se cierra la conexion

            
            //Para saber si el usuario y contrase�a son correctos:
            if ($this -> conexion -> numFilas() == 1)//Quiere decir que es correcto y encontro el usuario en la BD
            {  
                //Fetch_row me trae un arreglo de objetos, como s� que el
                //arreglo que me trae solo tiene una fila porque es
                //el id y el estado, entonces le indico la posicion [0] y [1]:
                $resultado = $this -> conexion -> extraer();
                
                $this -> idUsuario = $resultado[0];
                $this -> estado = $resultado[1];
                $this -> idRol = $resultado[2]; 
                               
                //Se genera reporte de LOG:
                $this -> datosUsuario();
                $log = new Log ("", "Autenticacion", "Correo: " . $this->correo, date("Y/m/d"), date("G:i:s"), $this->ip, $this->so, $this->idUsuario);
                $log -> crear();
                
                return true; //Puede entrar                 
            }
            else //No encontro el correo y clave en la BD
            {
                return false;
            }
        }
        
        //Metodo para consultar un usuario:
        function consultar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> consultar();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> consultar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $resultado = $this -> conexion -> extraer();
            $this -> nombre = $resultado[0];
            $this -> apellido = $resultado[1];
            $this -> tipo_docto = $resultado[2];
            $this -> num_docto = $resultado[3];
            $this -> correo = $resultado[4];
            $this -> clave = $resultado[5];  
            $this -> estado = $resultado[6];
            $this -> foto = $resultado[7];
            $this -> idRol = $resultado[8];
        }
        
        //Metodo para consultar todos los usuarios:
        function consultarTodos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> consultarTodos();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarTodos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $usuarios = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($usuarios, new Usuario ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], "", $resultado[6], $resultado[7]));
            }
            return $usuarios;
        }
        
        //Metodo para cambiar estado de un usuario:
        function cambiarEstado($estado)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> cambiarEstado($estado);
            $this -> conexion -> ejecutar($this -> usuarioDAO -> cambiarEstado($estado));
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Se genera reporte de LOG:
            $this -> consultar();
            $this -> datosUsuario();
            $log = new Log ("", "Cambio de Estado de Usuario", $this->datos, date("Y/m/d"), date("G:i:s"), $this->ip, $this->so, $_SESSION["id"]);
            $log -> crear();
        }
        
        
        //Metodo para editar la foto de un usuario:
        function editarFoto ()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql =  echo $this -> usuarioDAO -> editarFoto();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> editarFoto ());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            
            //Se genera reporte de LOG:
            $this -> consultar();
            $this -> datosUsuario();
            $antes="";
            
            if ($this -> idUsuario == $_SESSION["id"]) //Cambio de foto de su propio perfil
            {
                $this -> accion = "Edicion de Foto de Perfil";
            }
            else //Cambio de foto de un usuario x
            {
                $antes = "Cambio de Foto "; 
            }
      
            $log = new Log ("", $antes . $this->accion, $this->datos, date("Y/m/d"), date("G:i:s"), $this->ip, $this->so, $_SESSION["id"]);
            $log -> crear();
        }  
        
    }
?>