<?php
    //Para importar las clases que necesito usar:
    require "persistencia/CarritoDAO.php";
    
    class Carrito
    {
        private $idPedido;
        private $idProducto;
        private $cantidad_und;
        
        private $conexion;
        private $carritoDAO;
        
        
        //Constructor:
        
        function Carrito ($pIdPedido="", $pIdProducto="", $pCantidad_und="")
        {
            $this -> idPedido = $pIdPedido;
            $this -> idProducto = $pIdProducto;
            $this -> cantidad_und = $pCantidad_und;
            $this -> conexion = new Conexion();
            $this -> carritoDAO = new CarritoDAO ($pIdPedido, $pIdProducto, $pCantidad_und);
        }
        
        
        //Metodos GET:
        
        public function getIdPedido()
        {
            return $this->idPedido;
        }
        
        public function getIdProducto()
        {
            return $this->idProducto;
        }
        
        public function getCantidad_und()
        {
            return $this->cantidad_und;
        }

    

    
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear un carrito:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> carritoDAO -> crear();
            $this -> conexion -> ejecutar($this -> carritoDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        
        //Metodo para consultar un pedido que tenga el mismo idPedido e idProducto
        //Para cuando se quiera agregar un mismo producto en un mismo pedido:
        function consultar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> carritoDAO -> consultar();
            $this -> conexion -> ejecutar($this -> carritoDAO -> consultar());
            $this -> conexion -> cerrar(); //Se cierra la conexion            
            
            //Para saber si se quiere a�adir un mismo producto a un mismo pedido:
            if ($this -> conexion -> numFilas() == 1)
            {
                $resultado = $this -> conexion -> extraer();
                $this -> idPedido = $resultado[0];
                $this -> idProducto = $resultado[1];
                $this -> cantidad_und = $resultado[2]; 
                return true; 
            }
            else //No encontro ningun registro con el mismo idPedido e idProducto:
            {
                return false;
            }
        }  
       
        
        //Metodo para consultar todos los datos del carrito:
        function consultarTodos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> carritoDAO -> consultarTodos();
            $this -> conexion -> ejecutar($this -> carritoDAO -> consultarTodos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $carrito = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($carrito, new Carrito ($resultado[0], $resultado[1], $resultado[2]));
            }
            return $carrito;
        }
        
        //Metodo para editar la cantidad de unidades de producto de un carrito:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> carritoDAO -> editar();
            $this -> conexion -> ejecutar($this -> carritoDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        //Metodo para consultar la cantidad de productos dentro del carrito:
        function sumarProductos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> carritoDAO -> sumarProductos();
            $this -> conexion -> ejecutar($this -> carritoDAO -> sumarProductos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $resultado = $this -> conexion -> extraer();
            $this -> cantidad_und = $resultado[0]; 
            
            if ($this -> cantidad_und == NULL) //Si no hay resultados
            {
                $this -> cantidad_und = 0;
            }                                   
        }  
        
        //Metodo para eliminar un producto de carrito:
        function eliminar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> carritoDAO -> eliminar();
            $this -> conexion -> ejecutar($this -> carritoDAO -> eliminar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        //Metodo para eliminar todos los productos que hayan en un pedido:
        function eliminar2()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> carritoDAO -> eliminar2();
            $this -> conexion -> ejecutar($this -> carritoDAO -> eliminar2());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
    }
?>