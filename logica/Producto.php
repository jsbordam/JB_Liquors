<?php
    //Para importar las clases que necesito usar:
    require "persistencia/ProductoDAO.php";
    
    class Producto
    {
        private $idProducto;
        private $nombre;
        private $descripcion;
        private $foto;
        private $und_dis;
        private $valor;
        private $categoria;
        
        private $conexion;
        private $productoDAO;
        
        
        //Constructor:
        
        function Producto ($pIdProducto="", $pNombre="", $pDescripcion="", $pFoto="", $pUnd_dis="", $pValor="", $pCategoria="")
        {
            $this -> idProducto = $pIdProducto;
            $this -> nombre = $pNombre;
            $this -> descripcion = $pDescripcion;
            $this -> foto = $pFoto;
            $this -> und_dis = $pUnd_dis;
            $this -> valor = $pValor;
            $this -> categoria = $pCategoria;
            $this -> conexion = new Conexion();
            $this -> productoDAO = new ProductoDAO ($pIdProducto, $pNombre, $pDescripcion, $pFoto, $pUnd_dis, $pValor, $pCategoria);
        }
        
        
        //Metodos GET:
        
        public function getIdProducto()
        {
            return $this->idProducto;
        }
        
        public function getNombre()
        {
            return $this->nombre;
        }
        
        public function getDescripcion ()
        {
            return $this->descripcion;
        }
            
        public function getFoto ()
        {
            return $this->foto;
        }
            
        public function getUnd_dis ()
        {
            return $this->und_dis;
        }
            
        public function getValor ()
        {
            return $this->valor;
        }
        
        public function getCategoria ()
        {
            return $this->categoria;
        }
        
        
        
        //Para recoger algunos datos del usuario para el LOG:
        
        public $datos;
        public $ip;
        public $so;
        function datosProducto()
        {           
            //Para la categoria del producto:
            if ($this -> categoria == 1)
            {
                $categoria = "Aguardiente";
            }
            else if ($this -> categoria == 2)
            {
                $categoria = "Brandy";
            }
            else if ($this -> categoria == 3)
            {
                $categoria = "Cerveza";
            }
            else if ($this -> categoria == 4)
            {
                $categoria = "Champa�a";
            }
            else if ($this -> categoria == 5)
            {
                $categoria = "Ron";
            }
            else if ($this -> categoria == 6)
            {
                $categoria = "Tequila";
            }
            else if ($this -> categoria == 7)
            {
                $categoria = "Vino";
            }
            else if ($this -> categoria == 8)
            {
                $categoria = "Vodka";
            }
            else
            {
                $categoria = "Whisky";
            }
            
            //Datos que tendra el LOG:
            
            $this->datos =  "Nombre: " . $this->nombre .
            "   Descripcion: " . $this->descripcion .
            "   Und Disponibles: " . $this->und_dis .
            "   Valor: " . $this->valor .
            "   Categoria: " . $categoria;
            
            //Capturamos su Ip:
            $m = "";
            $externalContent = file_get_contents('http://checkip.dyndns.com/');
            preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
            
            $this->ip = $m[1];
            
            
            //Capturamos su Sistema Operativo (SO):
            $identificador = getenv("HTTP_USER_AGENT");
            
            
            function getPlatform($identificador)
            {
                $plataformas = array(
                    'Windows 10' => 'Windows NT 10.0+',
                    'Windows 8.1' => 'Windows NT 6.3+',
                    'Windows 8' => 'Windows NT 6.2+',
                    'Windows 7' => 'Windows NT 6.1+',
                    'Windows Vista' => 'Windows NT 6.0+',
                    'Windows XP' => 'Windows NT 5.1+',
                    'Windows 2003' => 'Windows NT 5.2+',
                    'Windows' => 'Windows otros',
                    'iPhone' => 'iPhone',
                    'iPad' => 'iPad',
                    'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
                    'Mac otros' => 'Macintosh',
                    'Android' => 'Android',
                    'BlackBerry' => 'BlackBerry',
                    'Linux' => 'Linux',
                );
                foreach ($plataformas as $plataforma=>$pattern)
                {
                    if (preg_match('/(?i)'.$pattern.'/', $identificador))
                        return $plataforma;
                }
                return 'Otras';
            }
            
            $this->so = getPlatform($identificador);
        }
        
        
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear un producto:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> productoDAO -> crear();
            $this -> conexion -> ejecutar($this -> productoDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Se genera reporte de LOG:
            $this -> datosProducto();
            
            $log = new Log ("", "Creacion de Producto", $this->datos, date("Y/m/d"), date("G:i:s"), $this->ip, $this->so, $_SESSION["id"]);
            $log -> crear();
        }
        
        //Metodo para consultar un producto:
        function consultar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> productoDAO -> consultar();
            $this -> conexion -> ejecutar($this -> productoDAO -> consultar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $resultado = $this -> conexion -> extraer();
            $this -> idProducto = $resultado[0];
            $this -> nombre = $resultado[1];
            $this -> descripcion = $resultado[2];
            $this -> foto = $resultado[3];
            $this -> und_dis = $resultado[4];
            $this -> valor = $resultado[5];
            $this -> categoria = $resultado[6];
        }
        
        
        //Metodo para consultar todos los productos:
        function consultarTodos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> productoDAO -> consultarTodos();
            $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $productos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($productos, new Producto ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]));
            }
            return $productos;
        }
        
        //Metodo para contar los productos de una categoria especifica:
        function contarPro()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> productoDAO -> contarPro();
            $this -> conexion -> ejecutar($this -> productoDAO -> contarPro());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Retorno el numero de resultados que seria el numero de productos que hay 
            //en esa categoria:
            return $this -> conexion -> numFilas();    
        }
        
        //Metodo para editar un producto:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> productoDAO -> editar();
            $this -> conexion -> ejecutar($this -> productoDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Se genera reporte de LOG:
            $this -> datosProducto();
            
            $log = new Log ("", "Edicion de Producto", $this->datos, date("Y/m/d"), date("G:i:s"), $this->ip, $this->so, $_SESSION["id"]);
            $log -> crear();
        }
        
        //Metodo para buscar filtro de producto:
        function buscar($filtro)
        {
            $this -> conexion -> abrir();
            //Para ver la consulta sql = echo $this -> productoDAO -> buscar($filtro);
            $this -> conexion -> ejecutar($this -> productoDAO -> buscar($filtro));
            $this -> conexion -> cerrar();
            $productos = array();
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($productos, new Producto ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]));
            }
            return $productos;
        } 
        
    }


?>