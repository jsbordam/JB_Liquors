<?php

    class PedidoDAO
    {
        private $idPedido;
        private $fecha_hora;
        private $valor_total;
        private $estado;
        private $idUsuario;
        
        
        //Constructor:
        
        function PedidoDAO ($pIdPedido="", $pFecha_hora="", $pValor_total="", $pEstado="", $pIdUsuario="")
        {
            $this -> idPedido = $pIdPedido;
            $this -> fecha_hora = $pFecha_hora;
            $this -> valor_total = $pValor_total;
            $this -> estado = $pEstado;
            $this -> idUsuario = $pIdUsuario;
        }
        
        //Metodos para las consultas en la BD:
        
        
        //Para crear:
        function crear()
        {
            return "INSERT into pedido (fecha_hora, valor_total, estado, idUsuario)
                    VALUES ('" . $this -> fecha_hora . "', '" . $this -> valor_total . "', '" . $this -> estado . "', '" . $this -> idUsuario . "')";
        }       
        
        //Para consultar un pedido que tenga estado 1 (SELECCIONADO):
        function consultar()
        {
            return "SELECT idPedido, estado
                    FROM pedido
                    WHERE idUsuario = '" . $this -> idUsuario . "' && estado = 1";
        }
        
        //Para consultar un pedido:
        function consultarUno()
        {
            return "SELECT idPedido, fecha_hora, valor_total, estado
                    FROM pedido
                    WHERE idPedido = '" . $this -> idPedido . "'";
        }
        
        
        //Para consultar todos los pedidos de un usuario:
        function consultarTodos()
        {
            return "SELECT idPedido, fecha_hora, valor_total, estado
                    FROM pedido
                    WHERE idUsuario = '" . $this -> idUsuario . "'";
        }
        
        //Para consultar todos los pedidos que existen:
        function consultarTodos2()
        {
            return "SELECT idPedido, fecha_hora, valor_total, estado, idUsuario
                    FROM pedido";
        }
        
        //Para editar:
        function editar()
        {
            return "UPDATE pedido
                    SET estado = '2', valor_total = '" . $this -> valor_total . "', fecha_hora = '" . $this -> fecha_hora ."'
                    WHERE idPedido = '" . $this -> idPedido . "'";
        }
        
        //Para eliminar un pedido:
        function eliminar()
        {
            return "DELETE FROM pedido
                    WHERE idPedido = '" . $this -> idPedido . "'";
        }
    }

?>
