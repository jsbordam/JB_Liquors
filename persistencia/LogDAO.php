<?php
    class LogDAO
    {
        private $idLog;
        private $accion;
        private $datos;
        private $fecha;
        private $hora;
        private $ip;
        private $so;
        private $idUsuario;
        
        
        //Constructor:
        function LogDAO ($pIdLog="", $pAccion="", $pDatos="", $pFecha="", $pHora="", $pIp="", $pSo="", $pIdUsuario="")
        {
            $this -> idLog = $pIdLog;
            $this -> accion = $pAccion;
            $this -> datos = $pDatos;
            $this -> fecha = $pFecha;
            $this -> hora = $pHora;
            $this -> ip = $pIp;
            $this -> so = $pSo;
            $this -> idUsuario = $pIdUsuario;
        }
        
        //Metodos para las consultas en la BD:
        
        
        //Para crear:
        function crear()
        {
            return "INSERT into log (accion, datos, fecha, hora, ip, so, idUsuario)
                    VALUES ('" . $this -> accion . "', '" . $this -> datos . "', '" . $this -> fecha . "', '" . $this -> hora . "', '" . $this -> ip . "', '" . $this -> so . "', '" . $this -> idUsuario . "')";
        }        
        
        
        //Para buscar accion log AJAX:
        function buscar($filtro)
        {
            return "SELECT idLog, accion, datos, fecha, hora, ip, so
                    FROM log
                    WHERE accion LIKE '%" . $filtro . "%' AND idUsuario = '" . $this -> idUsuario . "'
                    ORDER BY fecha DESC, hora DESC";
        }
        
    }
?>