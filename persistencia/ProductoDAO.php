<?php

    class ProductoDAO
    {
        private $idProducto;
        private $nombre;
        private $descripcion;
        private $foto;
        private $und_dis;
        private $valor;
        private $categoria;
        
        
        //Constructor:
        
        function ProductoDAO ($pIdProducto="", $pNombre="", $pDescripcion="", $pFoto="", $pUnd_dis="", $pValor="", $pCategoria="")
        {
            $this -> idProducto = $pIdProducto;
            $this -> nombre = $pNombre;
            $this -> descripcion = $pDescripcion;
            $this -> foto = $pFoto;
            $this -> und_dis = $pUnd_dis;
            $this -> valor = $pValor;
            $this -> categoria = $pCategoria;
        }
        
        //Metodos para las consultas en la BD:
        
        
        //Para crear:
        function crear()
        {
            return "INSERT into producto (nombre, descripcion, foto, und_dis, valor, categoria)
                    VALUES ('" . $this -> nombre . "', '" . $this -> descripcion . "', '" . $this -> foto . "', '" . $this -> und_dis . "', '" . $this -> valor . "', '" . $this -> categoria . "')";
        } 
        
        //Para consultar uno:
        function consultar()
        {
            return "SELECT idProducto, nombre, descripcion, foto, und_dis, valor, categoria
                    FROM producto
                    WHERE idProducto = '" . $this -> idProducto . "'";
        }
        
        //Para consultar todos:
        function consultarTodos()
        {
            return "SELECT idProducto, nombre, descripcion, foto, und_dis, valor, categoria
                    FROM producto";
        }
        
        //Para contar el numero de productos de una categoria:
        function contarPro()
        {
            return "SELECT idProducto
                    FROM producto
                    WHERE categoria = '" . $this -> categoria . "'";
        }
        
        //Para editar:
        function editar()
        {
            return "UPDATE producto
                    SET nombre = '". $this -> nombre . "', descripcion = '". $this -> descripcion . "', foto = '". $this -> foto . "', und_dis = '". $this -> und_dis . "', valor = '". $this -> valor . "', categoria = '". $this -> categoria . "'
                    WHERE idProducto = '" . $this -> idProducto . "'";
        }
        
        
        //Para buscar nombre producto AJAX:
        function buscar($filtro)
        {
            return "SELECT idProducto, nombre, descripcion, foto, und_dis, valor, categoria
                    FROM producto
                    WHERE nombre LIKE '%" . $filtro . "%'
                    ORDER BY nombre ASC";
        }
        
    }

?>
