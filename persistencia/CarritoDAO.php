<?php

    class CarritoDAO
    {
        private $idPedido;
        private $idProducto;
        private $cantidad_und;
        
        
        //Constructor:
        
        function CarritoDAO ($pIdPedido="", $pIdProducto="", $pCantidad_und="")
        {
            $this -> idPedido = $pIdPedido;
            $this -> idProducto = $pIdProducto;
            $this -> cantidad_und = $pCantidad_und;          
        }
        
        //Metodos para las consultas en la BD:
        
        //Para crear:
        function crear()
        {
            return "INSERT into carrito (idPedido, idProducto, cantidad_und)
                    VALUES ('" . $this -> idPedido . "', '" . $this -> idProducto . "', '" . $this -> cantidad_und . "')";
        }
        
        //Para consultar un pedido que tenga productos repetidos:
        function consultar()
        {
            return "SELECT idPedido, idProducto, cantidad_und
                    FROM carrito
                    WHERE idProducto = '" . $this -> idProducto . "' && idPedido = '" . $this -> idPedido . "'";
        }       
        
        //Para consultar todos:
        function consultarTodos()
        {
            return "SELECT idPedido, idProducto, cantidad_und
                    FROM carrito
                    WHERE idPedido = '" . $this -> idPedido . "'";
        }
        
        //Para editar:
        function editar()
        {
            return "UPDATE carrito
                    SET cantidad_und = '". $this -> cantidad_und . "'
                    WHERE idProducto = '" . $this -> idProducto . "' && idPedido = '" . $this -> idPedido . "'";
        }
        
        //Para sumar los productos que hay en el carrito:
        function sumarProductos()
        {
            return "SELECT SUM(cantidad_und)
                    FROM carrito
                    WHERE idPedido = '" . $this -> idPedido . "'";
        }
        
        
        //Para eliminar productos del carrito:
        function eliminar()
        {
            return "DELETE FROM carrito                
                    WHERE idProducto = '" . $this -> idProducto . "'";
        }
        
        //Para eliminar todos los productos de un pedido:
        function eliminar2()
        {
            return "DELETE FROM carrito
                    WHERE idPedido = '" . $this -> idPedido . "'";
        }
    }

?>
