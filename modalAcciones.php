<?php 
    require "logica/Usuario.php";
    require "persistencia/Conexion.php";
    //Consulta la lista de usuarios con un rol especifico:
    
    $usuario = new Usuario("", "", "", "", "", "", "", "", "", $_GET["rol"]);
    $usuarios = $usuario -> consultarTodos(); //Arreglo de Usuarios con rol: (1/2/3)
?>

<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel"><font color="black"><i class="fas fa-lock-open" data-toggle="tooltip" data-placement="bottom" title="Folder Abierto"></i></font></h5>
</div>

<div class="modal-body">
	<table>		
		<tr>
    		<td>
        		<a href="#" class="btn float-right modal_btn3">
        			<i id="cambiarEstado<?php echo $_GET["idUsu"]?>"
        				 class='fas fa-user-cog' data-toggle='tooltip' data-placement='bottom'
        				  title='Cambiar Estado'></i>
        	    </a> 	
    	    </td>
	    </tr>
	    
	    
	    <tr>
    		<td>
        		<a href="#" class="btn float-right modal_btn3">
        			<i id='editarUsuario" . $usuarioActual -> getIdUsuario(). "' 
        				class='fas fa-user-edit' data-toggle='tooltip' data-placement='bottom'
        				 title='Editar Usuario'></i>
        		</a>	
    		</td>
	    </tr>
		
		
		<tr>
    		<td>
        		<a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/editarFotoUsuario.php")?>&m&idUsu=<?php echo $_GET["idUsu"]?>" class="btn float-right modal_btn3">
        			<i class='fas fa-camera-retro' data-toggle='tooltip' data-placement='bottom'
        			 	title='Cambiar Foto'></i>
        	 	</a>
	 		</td>
	    </tr>
	 </table>			 			
</div>

<div class="modal-footer">
	<h5 class="text-center"><button type="button" class="btn float-right modal_btn3" data-dismiss="modal"><i class="fas fa-times"></i></button></h5>
</div>



<!--Este es el JQuery para cambiar estado: -->
<script>
    $(document).ready(function()
    {
        <?php 
            foreach ($usuarios as $usuarioActual)
            {
                echo "$(\"#cambiarEstado". $usuarioActual -> getIdUsuario() ."\").click(function()\n";
                echo "{\n";
                //Definimos la ruta en donde se recargara esta capa y la encriptamos respectivamente:
                echo "    url = \"indexAjax.php?pid=" . base64_encode("presentacion/administrador/cambiarEstadoUsuarioAjax.php") . "&idUsu=" . $usuarioActual -> getIdUsuario() . "\"\n";
                echo "    $(\"#estado" . $usuarioActual -> getIdUsuario() . "\").load(url);\n";
                echo "});\n\n";
            }                  
        ?> 
    });
</script>