<div class="modal-body">
	<section id="portfolio">
		<div class="row">
			<div class="col-md-12" align="center">
				<div class="card3">
					<div class="row">
						<div class="col-md-3" align="right">
							<div class="card-header">
								<font face="Arial Black" size="5"> <i class="fas fa-search"></i>
									Buscar
								</font>
							</div>
						</div>

						<div class="col-md-9" align="left">
							<div class="card-header">
								<div class="form-group">
									<input type="text" id="filtro" class="form-control"
										placeholder="Nombre producto">
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<br><br>

		<div class="row mt-3" id="resultados">
			
		</div>
	</section>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-warning modal_btn3 btn-block"
		data-dismiss="modal">Cerrar</button>
</div>


<!--Este es el JQuery para buscar Producto: -->
<script>
    $(document).ready(function(){
    	$("#filtro").keyup(function(){
    		if($("#filtro").val().length > 0)
        	{
            	//Encriptamos la ruta para que no sea visible en la inspección de codigo:
    			url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/productos/buscarProductosAjax.php") ?>&filtro=" + $("#filtro").val();
    			$("#resultados").load(url);
    		} 
    	});
    });
</script>
