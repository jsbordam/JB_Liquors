<!-- Estas lineas es para implementar tooltip (Peque�as etiquetas descriptivas de botones) -->
<script type="text/javascript">
	$(function () 
	{
      $('[data-toggle="tooltip"]').tooltip()
    })
</script>   
<?php  
    //Para recuperar la variable sesion cuando se recarga la pagina:
    session_start();
    require "logica/Usuario.php";
    require "logica/Producto.php";
    require "logica/Pedido.php";
    require "logica/Carrito.php";
    require "logica/Log.php";
    require "persistencia/Conexion.php";
    
    //Zona horaria en colombia:
    date_default_timezone_set('America/Bogota'); 
    
    //Desencriptamos el pid para que lo pueda leer:
    $pid = base64_decode($_GET["pid"]);
    include $pid;
    
?>