<?php include "presentacion/menuPrincipal/menu.php";?>
<!--==========================
      Seccion de inicio:
      ============================-->
          
<section id="portfolio">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-12">
				<h3 class="section-title">Inicio</h3>
				<div class="section-title-divider"></div>
				<h4 class="section-description">Estas en nuestra tienda virtual de
					licores, que quieres pedir hoy?</h4>
			</div>
		</div>
		
		<div class="row">
			<section id="slider">
				<ul>
					<li><img src="img/4.png"></li>
					<li><img src="img/2.png"></li>
					<li><img src="img/5.png"></li>
					<li><img src="img/3.png"></li>
				</ul>
			</section>
		</div>

	</div>


	<div class="container wow fadeInUp">
		<div class="row mt-5">
			<div class="col-md-6 text-center">
				<img src="img/imagen4.jpg" width="100%">
			</div>

			<div class="col-md-6 text-center">
				<img src="img/imagen2.jpg" width="100%">
			</div>
		</div>
	</div>
	<br>
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-6 text-center">
				<img src="img/imagen3.jpg" width="100%">
			</div>

			<div class="col-md-6 text-center">
				<img src="img/imagen5.jpg" width="100%">
			</div>
		</div>
	</div>
</section>






<!--==========================
  Subscribe Section
  ============================-->
<section id="subscribe">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-8">
				<h3 class="subscribe-title">GRACIAS POR VISITARNOS</h3>
				<p class="subscribe-text">Recuerda que estamos siempre a tu
					servicio, dispuestos a brindarte los mas altos estandares de
					atencion y calidad en tus transacciones.</p>
			</div>
			<div class="col-md-4 subscribe-btn-container">
				<a class="subscribe-btn"
					href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>&pre">VUELVE
					PRONTO</a>
			</div>
		</div>
	</div>
</section>


<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

