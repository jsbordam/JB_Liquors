<!--Esta es la animaci�n de carga: -->
<div id="preloader"></div>

<!--==========================
      Secci�n principal
      ============================-->
<section id="hero">
	<div class="hero-container">
		<div class="wow fadeIn">

			<div class="hero-logo">
				<img src="img/imagen1.jpg" width="300px">
			</div>

			<h1>
				<font face="Algerian" COLOR="white">BIENVENIDO A JB LIQUORS</font>
			</h1>
			<br> <br>
			<h2>
				Aqu&iacute; encontrar&aacute;s: <span class="rotating">los mejores licores, el
					mejor precio, la mayor calidad</span>
			</h2>
			<div class="actions">
				<a href="#portfolio" class="btn-warning"> <font face="Arial Black"
					COLOR="black">EMPEZAR</font>
				</a>
			</div>
		</div>
	</div>
</section>
