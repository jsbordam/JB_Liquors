<?php 
    $producto = new Producto();
    $productos = $producto -> consultarTodos();
    
    $error = 0;
    if (isset($_GET["error"]))
    {
        $error = $_GET["error"];
    }

    if (isset($_GET["editar"])) //Se quiere editar un producto
    {
        $idProducto = $_GET["idPro"]; //Llega por url
        //Estos son los datos que llegan por el formulario cuando se quiere editar:        
        $nombre = $_POST["nombre"];
        $descripcion = $_POST["descripcion"];
        $foto = $_POST["foto"];
        $und_dis = $_POST["und_dis"];
        $valor = $_POST["valor"];
        
        //Para categoria:
        if ($_POST["categoria"] == "Aguardiente")
        {
            $categoria = 1;
        }
        else if ($_POST["categoria"] == "Brandy")
        {
            $categoria = 2;
        }
        else if ($_POST["categoria"] == "Cerveza")
        {
            $categoria = 3;
        }
        else if ($_POST["categoria"] == "Ron")
        {
            $categoria = 5;
        }
        else if ($_POST["categoria"] == "Tequila")
        {
            $categoria = 6;
        }
        else if ($_POST["categoria"] == "Vino")
        {
            $categoria = 7;
        }
        else if ($_POST["categoria"] == "Vodka")
        {
            $categoria = 8;
        }
        else if ($_POST["categoria"] == "Whisky")
        {
            $categoria = 9;
        }
        else 
        {
            $categoria = 4; //Para categoria CHAMPA�A
        }
        
        
        //Envio esos datos nuevos que tendra el producto:
        $producto = new Producto ($idProducto, $nombre, $descripcion, $foto, $und_dis, $valor, $categoria);
        $producto -> editar();
 
        
        //Actualizo la lista de productos
        $productos = $producto -> consultarTodos();
    }
?>

<!--==========================
  Porfolio Section
  ============================-->
<section id="portfolio">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-6">
				<a
					href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/nuestrosProductos.php")?>&m"
					class='btn float-left ver_btn'> VER POR CATALOGO </a>
			</div>
			<div class="col-md-6">
				<a
					href="modalProducto.php" data-toggle="modal" data-target="#modalProductos"
					class='btn float-right ver_btn'> VER POR FILTRO </a>
			</div>		
		</div>
		
		<br><br>
		
		<div class="row">
			<div class="col-md-12">
				<h3 class="section-title">Nuestros Productos</h3>
				<div class="section-title-divider"></div>
				<?php 
				    if (($_GET["categoria"]) == 1) //Seccion de AGUARDIENTE
				    {
				        echo "<p class='section-description'>Seccion de AGUARDIENTE</p>";
				    }
				    else if (($_GET["categoria"]) == 2) //Seccion de BRANDY
				    {
				        echo "<p class='section-description'>Seccion de BRANDY</p>";
				    }				
				    else if (($_GET["categoria"]) == 3) //Seccion de CERVEZA
				    {
				        echo "<p class='section-description'>Seccion de CERVEZA</p>";
				    }				
				    else if (($_GET["categoria"]) == 4) //Seccion de CHAMPA�A
				    {
				        echo "<p class='section-description'>Seccion de CHAMPA&Ntilde;A</p>";
				    }
				    else if (($_GET["categoria"]) == 5) //Seccion de RON
				    {
				        echo "<p class='section-description'>Seccion de RON</p>";
				    }
				    else if (($_GET["categoria"]) == 6) //Seccion de TEQUILA
				    {
				        echo "<p class='section-description'>Seccion de TEQUILA</p>";
				    }
				    else if (($_GET["categoria"]) == 7) //Seccion de VINO
				    {
				        echo "<p class='section-description'>Seccion de VINO</p>";
				    }
				    else if (($_GET["categoria"]) == 8) //Seccion de VODKA
				    {
				        echo "<p class='section-description'>Seccion de VODKA</p>";
				    }
				    else if (($_GET["categoria"]) == 9) //Seccion de WHISKY
				    {
				        echo "<p class='section-description'>Seccion de WHISKY</p>";
				    }
				?>
				
			</div>
		</div>
		
		<!-- Primero se valida si existe el error de la sesion inactiva -->
		<?php
		  if ($error == 1) 
		  {
        ?>      
            <div class="alert alert-dismissible fade show" role="alert">				
				<strong>
					<i class="fas fa-exclamation-triangle"></i> No se pueden a&ntilde;adir productos al carrito, primero debes 
					<a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/iniciarSesion.php")?>" class="alert-link">Iniciar Sesion</a>
				</strong>						
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>			
        <?php
            }
        ?>
        
        <!-- Para avisar cuando el producto sea agregado al carrito -->
        <?php
            if(isset($_GET["agregado"]))
            { 
        ?>  
            <section id="alertPro">          
           		<div class="alert alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-check-circle"></i> Genial: Has agregado un producto a tu carrito correctamente!</strong> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    	<span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </section>          
        <?php 
            }
        ?>
        
        <!-- Para avisar cuando el producto sea editado -->
        <?php
            if(isset($_GET["editar"]))
            { 
        ?>  
            <section id="alertPro">          
           		<div class="alert alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-check-circle"></i> Genial: Has editado el producto correctamente!</strong> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    	<span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </section>          
        <?php 
            }
        ?>



		<?php 
	      //Si el rol de la sesion es (cliente) entonces le muestra los productos con
	      //el servicio de la transaccion con el carrito:
	      
		  if ($_SESSION["rol"] == "Cliente" || $_SESSION["id"] == NULL)
		  {
		?>
		
		<div class="row">
			<?php	
    			foreach ($productos as $productoActual)
    			{   			    
    			    if ($productoActual -> getCategoria() == $_GET["categoria"])
    			    {  			        
    			        echo "<div class='col-md-3'>";
    			            echo "<br>";
        			        echo "<div class='card2'>";
            			        echo "<a class='portfolio-item'
                					           style='background-image: url(" . $productoActual -> getFoto() . ");'
                					           href='index.php?pid=" . base64_encode("presentacion/cliente/carritoProductos.php") . "&m&verificar&categoria=" . $_GET["categoria"] . "&idPro=" . $productoActual -> getIdProducto() ."'" . ">";
            			        echo "<div class='details'>";
                			        echo "<h4 class='card-title'>";
            			                 echo "<font face='Algerian'>" . $productoActual -> getIdProducto() . "</font>";
                			        echo "</h4>";
                			        echo "<h4 class='card-title'>";
                			             echo "<font face='Algerian'>" . $productoActual -> getNombre() . " " . $productoActual -> getDescripcion(). "</font>";
                			        echo "</h4>";
                			        echo "<br><br><br>";
                			        echo "<h2 class='card-text'>";
                			             echo "<font face='Arial Black'>A&ntilde;adir al carrito</font>";
                			        echo "</h2>";
                			        echo "<h2 class='card-carrito'><i class='fas fa-cart-arrow-down'></i></h2>";
            			        echo "</div>";
            			        echo "</a>";
            			        echo "<div class='card-body'>";
                			        echo "<h4 class='card-title'>";
                			             echo "<a href='index.php?pid=" . base64_encode("presentacion/cliente/descripcionProducto.php") . "&m&verificar&categoria=" . $_GET["categoria"] . "&idPro=" . $productoActual -> getIdProducto() ."'>
                                                    <font face='Algerian' color='white'>" . $productoActual -> getNombre() . " " . $productoActual -> getDescripcion(). "
                                                    </font>
                                              </a>";
                			        echo "</h4>";
                			        echo "<h3 class='card-text2'";
                			        echo "<font face='Arial'>$" . number_format($productoActual -> getValor(), ...array(0, ',', '.')) . "</font>";
                			        echo "</h3>";
            			        echo "</div>";
        			        echo "</div>";       			        
    			        echo "</div>";
    			    } 
    			    
    			}
    	    ?>
		</div>
		<?php 
		  }
		  else //Es porque el rol de la sesion es (Admin/Proveedor)
		  {
		      //Entonces le muestra solo los productos:
		?>
		<div class="row">
			<?php		     
    			foreach ($productos as $productoActual)
    			{   			    
    			    if ($productoActual -> getCategoria() == $_GET["categoria"])
    			    {
    			        echo "<div class='col-md-3'>";
    			            echo "<br>";
        			        echo "<div class='card2'>";
            			        //Opcion solo para el Admin:
            			        if ($_SESSION["rol"] == "Administrador")
            			        {
                			        echo "<a class='portfolio-item'
                    					           style='background-image: url(" . $productoActual -> getFoto() . ");'
                    					           href='index.php?pid=" . base64_encode("presentacion/administrador/editarProducto.php") . "&m&idPro=" . $productoActual -> getIdProducto() ."&categoria=" . $_GET["categoria"] . "'>";
            			        }
            			        else //Si es proveedor
            			        {
            			            echo "<a class='portfolio-item'
                    					           style='background-image: url(" . $productoActual -> getFoto() . ");'
                    					           href='#'>";
            			        }
            			        echo "<div class='details'>";
                			        echo "<h4 class='card-title'>";
            			                 echo "<font face='Algerian'>" . $productoActual -> getIdProducto() . "</font>";
                			        echo "</h4>";
                			        echo "<h4 class='card-title'>";
                			             echo "<font face='Algerian'>" . $productoActual -> getNombre() . " " . $productoActual -> getDescripcion(). "</font>";
                			        echo "</h4>";
                			        echo "<br><br><br>";
                			       
                			        //Opcion solo para el Admin:
                			        if ($_SESSION["rol"] == "Administrador")
                			        {
                			            echo "<h2 class='card-text'>";
                			                 echo "<font face='Arial Black'>Editar Producto</font>";
                			            echo "</h2>";
                			            echo "<h2 class='card-carrito'><i class='fas fa-edit'></i></h2>";
                			        }
                			         			    
                			    echo "</div>";
            			        echo "</a>";
            			        echo "<div class='card-body'>";
                			        echo "<h4 class='card-title'>";
                			             echo "<font face='Algerian'>" . $productoActual -> getNombre() . " " . $productoActual -> getDescripcion(). "</font>";
                			        echo "</h4>";
                			        echo "<h3 class='card-text2'";
                			        echo "<font face='Arial'>$" . number_format($productoActual -> getValor(), ...array(0, ',', '.')) . "</font>";
                			        echo "</h3>";
            			        echo "</div>";
        			        echo "</div>";
    			        echo "</div>";   		
    			    }   			    	  
    			}
    	    ?>
		</div>
		<?php 
		  }
		?>
		
		<br>						
	</div>
</section>

<?php 
  //Si el rol de la sesion es (cliente): 
  if ($_SESSION["rol"] == "Cliente")
  {
?>
<!--==========================
  Subscrbe Section
  ============================-->
<section id="subscribe">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-8">
				<h3 class="subscribe-title">GRACIAS POR VISITARNOS</h3>
				<p class="subscribe-text">Recuerda que estamos siempre a tu
					servicio, dispuestos a brindarte los mas altos estandares de
					atencion y calidad en tus compras.</p>
			</div>
			<div class="col-md-4 subscribe-btn-container">
				<a class="subscribe-btn"
					href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>&pre">VUELVE
					PRONTO</a>
			</div>
		</div>
	</div>
</section>
<?php 
  }
?>

<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>


<!--Este es el Modal de Productos: -->

<section id="modalPro">	
    <div class="modal fade" id="modalProductos" tabindex="-1"
    	aria-labelledby="exampleModalLabel" aria-hidden="true"> 
    	<div class="modal-dialog">
    		<div class="modal-content" id="modal-content"></div>
    	</div>
    </div>
</section>  

<!--Este JavaScript es para el Modal de Productos: -->
<script>
$('body').on('show.bs.modal', '.modal', function (e) {
	var link = $(e.relatedTarget);
	$(this).find(".modal-content").load(link.attr("href"));
});
</script>