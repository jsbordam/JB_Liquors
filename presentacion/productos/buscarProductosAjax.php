<?php	    
    $filtro = $_GET["filtro"];
    
    //Traigo el arreglo de registros del LOG asociados a ese filtro:
    $producto = new Producto();
    $arregloProductos = $producto -> buscar ($filtro);
    
    //Para resaltar el filtro que se esta buscando:
    
    //Convierte toda la cadena a minusculas
    $filtro = strtolower ($filtro);
    //Convierte la primera letra en Mayuscula
    $filtro = ucwords($filtro);
    
    if ($_SESSION["rol"] == "Cliente" || $_SESSION["id"] == NULL) //Solo para cliente
    {
        foreach ($arregloProductos as $productoActual)
        {
            echo "<div class='col-md-3'>";
            echo "<br>";
            echo "<div class='card2'>";
            echo "<a class='portfolio-item'
		              style='background-image: url(" . $productoActual -> getFoto() . ");'
		              href='index.php?pid=" . base64_encode("presentacion/cliente/descripcionProducto.php") . "&m&verificar&categoria=" . $productoActual -> getCategoria() . "&idPro=" . $productoActual -> getIdProducto() ."'" . ">";
            echo "<div class='details'>";
            echo "<h4 class='card-title'>";
            echo "<font face='Algerian'>" . $productoActual -> getIdProducto() . "</font>";
            echo "</h4>";
            echo "<h4 class='card-title'>";
            echo "<font face='Algerian'>" . $productoActual -> getNombre() . " " . $productoActual -> getDescripcion(). "</font>";
            echo "</h4>";
            echo "<br><br><br>";
            echo "<h2 class='card-text'>";
            echo "<font face='Arial Black'>A&ntilde;adir al carrito</font>";
            echo "</h2>";
            echo "<h2 class='card-carrito'><i class='fas fa-cart-arrow-down'></i></h2>";
            echo "</div>";
            echo "</a>";
            echo "<div class='card-body'>";
            echo "<h4 class='card-title'>";
            echo "<a href='index.php?pid=" . base64_encode("presentacion/cliente/descripcionProducto.php") . "&m&verificar&categoria=" . $productoActual -> getCategoria() . "&idPro=" . $productoActual -> getIdProducto() ."'>
                                   <font face='Algerian' color='white'>" . str_replace($filtro, "<font color='red'>" . $filtro . "</font>", $productoActual -> getNombre()) . " " . $productoActual -> getDescripcion(). "
                                   </font>
                             </a>";
            echo "</h4>";
            echo "<h3 class='card-text2'";
            echo "<font face='Arial'>$" . number_format($productoActual -> getValor(), ...array(0, ',', '.')) . "</font>";
            echo "</h3>";
            echo "</div>";
            echo "</div>";
            echo "</div>";
        }
    }
    else //Para admi y proveedor
    {
        foreach ($arregloProductos as $productoActual)
        {
            echo "<div class='col-md-3'>";
            echo "<br>";
            echo "<div class='card2'>";
            //Opcion solo para el Admin:
            if ($_SESSION["rol"] == "Administrador")
            {
                echo "<a class='portfolio-item'
    		           style='background-image: url(" . $productoActual -> getFoto() . ");'
    		           href='index.php?pid=" . base64_encode("presentacion/administrador/editarProducto.php") . "&m&idPro=" . $productoActual -> getIdProducto() ."&categoria=" . $productoActual -> getCategoria() . "'>";
            }
            else //Si es proveedor
            {
                echo "<a class='portfolio-item'
			           style='background-image: url(" . $productoActual -> getFoto() . ");'
			           href='#'>";
            }
            echo "<div class='details'>";
            echo "<h4 class='card-title'>";
            echo "<font face='Algerian'>" . $productoActual -> getIdProducto() . "</font>";
            echo "</h4>";
            echo "<h4 class='card-title'>";
            echo "<font face='Algerian'>" . $productoActual -> getNombre() . " " . $productoActual -> getDescripcion(). "</font>";
            echo "</h4>";
            echo "<br><br><br>";
            
            //Opcion solo para el Admin:
            if ($_SESSION["rol"] == "Administrador")
            {
                echo "<h2 class='card-text'>";
                echo "<font face='Arial Black'>Editar Producto</font>";
                echo "</h2>";
                echo "<h2 class='card-carrito'><i class='fas fa-edit'></i></h2>";
            }
  
            echo "</div>";
            echo "</a>";
            echo "<div class='card-body'>";
            echo "<h4 class='card-title'>";
            echo "<font face='Algerian' color='white'>" . str_replace($filtro, "<font color='red'>" . $filtro . "</font>", $productoActual -> getNombre()) . " " . $productoActual -> getDescripcion(). "
                                   </font>";
            echo "</h4>";
            echo "<h3 class='card-text2'";
            echo "<font face='Arial'>$" . number_format($productoActual -> getValor(), ...array(0, ',', '.')) . "</font>";
            echo "</h3>";
            echo "</div>";
            echo "</div>";
            echo "</div>";
        }
    }
    
?>


