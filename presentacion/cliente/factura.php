<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Cliente") //Funcion solo para admi
    {
?>
<?php
    //Primero valido si tiene productos en el carrito y de ser asi los elimino:
    $carrito = new Carrito ($_GET["idPedido"]);
    $carrito -> sumarProductos(); //Sumo los productos que tienen ese pedido

    if (isset($_GET["eliminar"])) //Cuando se quiere eliminar un pedido
    { 
        
        if ($carrito -> getCantidad_und() > 0)
        {
            $carrito -> eliminar2(); //Elimino todos los productos asociados a ese pedido
        }
        
        //Ahora si puedo eliminar el pedido:
        $pedido = new Pedido ($_GET["idPedido"]);
        $pedido -> eliminar();
    }
    
    
    //Me trae un array de todos los registros asociados a ese pedido
    $arregloCarrito = $carrito -> consultarTodos(); 
?>

<section id="portfolio">
	<div class="container wow fadeInUp">
		<h3 class="section-title">Factura Generada</h3>
		<div class="section-title-divider"></div>
		<br><br>
		<table class="table table-info table-striped table-hover">
			<thead>
				<tr>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Codigo</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Imagen</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Cantidad</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Nombre de Producto</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Precio Unitario</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Total</font></h5></th>
				</tr>
			</thead>
			<tbody>
            	<?php 
                    $valorTotal=0;
                	foreach ($arregloCarrito as $registroActual)
                	{      	    
                	    //Busco los datos del producto de cada registro:
                	    $producto = new Producto ($registroActual->getIdProducto());
                	    $producto -> consultar();
                	    
                	    $valorCantidad = $producto->getValor() * $registroActual->getCantidad_und();
                	    $valorTotal += $valorCantidad;
                	    
                	    //Muestro los datos del producto que necesito:
                	    echo "<tr>";
                	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $producto->getIdProducto() . "</font></h5></td>";             	        
                	        echo "<td class='text-center'><img src='" . $producto->getFoto() . "' width='100px' height='100px'></td>";  
                	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $registroActual->getCantidad_und() . "</font></h5></td>";
                	        echo "<td ><font face='Arial' size='3' color='black'>" . $producto->getNombre() . " " . $producto->getDescripcion() . "</font></td>";               	        
                	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>$" . number_format($producto->getValor(), ...array(0, ',', '.')) . "</font></h5></td>";
                	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>$" . number_format($valorCantidad, ...array(0, ',', '.')) . "</font></h5></td>";       	       
                	   echo "</tr>";                	   
                	} 
                	echo "<tr>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td><h5><font face='Arial Black' size='4' color='black'>Sub-Total:</td></font></h5>";
                    	echo "<td><h5><font face='Arial Black' size='4' color='black'>$" . number_format($valorTotal, ...array(0, ',', '.')) . "</td></font></h5>";
                	echo "</tr>"; 
                	
                	echo "<tr>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td><h5><font face='Arial Black' size='4' color='black'>Total:</td></font></h5>";
                    	echo "<td><h5><font face='Arial Black' size='4' color='black'>$" . number_format($valorTotal, ...array(0, ',', '.')) . "</td></font></h5>";
                	echo "</tr>"; 
            	?>
    	</tbody>
	</table>
	<br>
	<?php 
	  //Si hay productos en el carrito muestra la factura
	  if ($valorTotal > 0) 
	  {
	?>
	<div class="row">
    	<div class="col-md-3">		
        		<img src="https://static-unitag.com/images/help/QRCode/qrcode.png?mh=07b7c2a2" width="200px" height="200px" data-toggle="tooltip" title="CODIGO QR">
    	</div>
		<div class="col-md-9">		
    		<a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/realizarPedido.php")?>&m&idPedido=<?php echo $_GET["idPedido"]?>&valorTotal=<?php echo $valorTotal?>" class='btn float-right login_btn3'>
    			REALIZAR PEDIDO
    		</a>
    	</div>
	</div>
	<?php 
		  }
	?>
	<br>
	<!-- Para avisar cuando el pedido se elimine -->
    <?php
        if(isset($_GET["confirmar"]))
        { 
    ?>  
        <section id="alertPro">          
       		<div class="alert alert-dismissible fade show" role="alert">
                <strong><i class="fas fa-check-circle"></i> Se ha cancelado el pedido correctamente!</strong> 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                	<span aria-hidden="true">&times;</span>
                </button>
            </div>
        </section>          
    <?php 
        }
    ?>
	<div class="row">
    	<div class="container wow fadeInUp">
    		<h3 class="section-title">Pedido Generado</h3>
    		<div class="section-title-divider"></div>
    		<br><br>
    		<?php 
        		//Traigo el pedido de ese usuario:
        		$pedido = new Pedido ($_GET["idPedido"]);
        		if ($pedido -> consultarUno()) //Si ese pedido existe lo muestra
        		{
    		?>
    		<table class="table table-danger table-striped table-hover">
    			<thead>
    				<tr>
    					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Codigo</font></h5></th>
    					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Fecha - Hora</font></h5></th>
    					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Valor Total</font></h5></th>
    					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Productos</font></h5></th>
    					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Estado</font></h5></th>
    					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Accion</font></h5></th>
    				</tr>
    			</thead>
    			<tbody>
                	<?php  
                	        //Busco el carrito de productos que tiene registrado ese idPedido
                	        $carrito = new Carrito ($_GET["idPedido"]);
                	        
                	        //Me trae un array de todos los registros asociados a ese pedido
                	        $arregloCarrito = $carrito -> consultarTodos();
                	        
                	        $unidadesTotal=0;
                	        //Para calcular cuantos productos pidio en ese pedido
                	        foreach ($arregloCarrito as $carritoActual)
                	        {
                	            $unidadesTotal += $carritoActual -> getCantidad_und();
                	        }
                	        
                	        //Muestro los datos del pedido que necesito:
                	        echo "<tr>";
                    	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedido->getIdPedido() . "</font></h5></td>";
                    	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>Esperando...</font></h5></td>";
                    	        echo "<td class='text-center'><font face='Arial' size='3' color='black'>Esperando...</font></td>";
                    	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $unidadesTotal . " Unidades" . "</font></h5></td>";
                    	        
                    	        if ($pedido->getEstado() == 1)//Seleccionando
                    	        {
                    	            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>SELECCIONANDO <font size='4'><i class='fas fa-hourglass-half'></i></font></font></h5></td>";
                    	        }
                    	        else//Solicitado
                    	        {
                    	            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>SOLICITADO <font size='4'><i class='fas fa-clipboard-check'></i></font></font></h5></td>";
                    	        }
                    	        echo "<td class='text-center'><a href='modalPedido.php?idPedido=" . $_GET["idPedido"] . "&eliminar' data-toggle='modal' data-target='#modalPedido'><font size='5' color='red'><i class='fas fa-trash-alt' data-toggle='tooltip' data-placement='bottom' title='Cancelar'></i></font></td>";
                	        echo "</tr>";            	      	                   	                         	
                	?>
            	</tbody>
        	</table>
        	<?php 
        		}
            	else
            	{
            	    //El pedido no existe:
            	    echo "<h2 class='text-center'>No se ha generado ningun pedido todavia...</h2>";
            	}   
        	?>  	
    	</div>
	</div>
	<br><br>
	<div class="row">
		<div class="col-md-10">		
    		<a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/factura.php")?>&m&idPedido=<?php echo $_GET["idPedido"]?>" class='btn float-right login_btn3'>
    			ACTUALIZAR
    		</a>
    	</div>
	</div>
		
	<br><br><br><br><br><br><br><br>			
	</div>
</section>

<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>	

<!--Este es el Modal de Pedido: -->

<section id="modalP">	
    <div class="modal fade" id="modalPedido" tabindex="-1"
    	aria-labelledby="exampleModalLabel" aria-hidden="true"> 
    	<div class="modal-dialog">
    		<div class="modal-content" id="modal-content"></div>
    	</div>
    </div>
</section>  

<!--Este JavaScript es para el Modal de Pedido: -->
<script>
$('body').on('show.bs.modal', '.modal', function (e) {
	var link = $(e.relatedTarget);
	$(this).find(".modal-content").load(link.attr("href"));
});
</script>

<?php 
    if (isset($_GET["eliminar"]))
    { 
?>
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/cliente/factura.php")?>&m&confirmar&idPedido=<?php echo $_GET["idPedido"]?>"/>
<?php }?>


<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"/>
<?php
}
?>