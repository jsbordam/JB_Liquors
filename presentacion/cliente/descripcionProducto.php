<?php 
    $producto = new Producto($_GET["idPro"]);
    $producto -> consultar();
     
    
    echo "<section id='about'>";
        echo "<div class='container wow fadeInUp'>";
            echo "<div class='row'>";
                echo "<div class='col-md-12'>";
                    echo "<h5 class='section-title'>";
                        echo "<font face='Algerian'>" . "Codigo: " . $producto -> getIdProducto() . "</font>";
                    echo "</h5>";
                    echo "<h4 class='section-title'>";
                        echo "<font face='Algerian' size='6'>" . $producto -> getNombre() . " " . $producto -> getDescripcion(). "</font>";
                    echo "</h4>";
                    echo "<div class='section-title-divider'></div>";
                echo "</div>";
            echo "</div>";
            echo "<div class='row mt-5'>";
                echo "<div class='col-md-6 text-center'>";
                    echo "<img src='" . $producto -> getFoto() . "' width='400px' height='400px'>";
                echo "</div>";
                echo "<div class='col-md-6 text-center'>";
                    echo "<div class='row'>";
                        echo "<div class='col-md-12'>";
                        echo "<h2>" . "$" . number_format($producto -> getValor(), ...array(0, ',', '.')) . "</h2>";
                        echo "</div>";
                    echo "</div>";
                    echo "<br>";
                    echo "<div class='row'>";
                        echo "<div class='col-md-4 text-center'>";
                            echo "<font color='red'><i class='fas fa-beer'></i></font>";
                            echo "<br><br>";
                            echo "<h3><b>Presentacion:</b></h3>";
                            echo "<h3>" . $producto -> getDescripcion() . "</h3>";
                        echo "</div>";
                        echo "<div class='col-md-4 text-center'>";
                            echo "<font color='red'><i class='fas fa-flag'></i></font>";
                            echo "<br><br>";
                            echo "<h3><b>Pais:</b></h3>";
                            echo "<h3>Colombia</h3>";
                        echo "</div>";
                        echo "<div class='col-md-4 text-center'>";                       
                            echo "<font color='red'><i class='fas fa-hand-holding-usd'></i></font>";
                            echo "<br><br>";
                            echo "<h3><b>Envio:</b></h3>";
                            echo "<h3>Gratis</h3>";
                        echo "</div>";
                    echo "</div>";
                    echo "<br><br>";
                    echo "<div class='row'>";
                        echo "<div class='col-md-4'>";
?>                      
                            <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/cliente/carritoProductos.php") . "&m&idPro=" . $_GET["idPro"] . "&categoria=" . $_GET["categoria"]?> method="post">
                                <div class='form-group'>
                                    <h3><b>Cantidad:</b></h3>
                                    <input type='number' value='1' name='cantidad' required='required'>
                                </div>
                                <div class='form-group'>
                                    <button type='submit' name="agregar" class='btn float-right login_btn4'>
                                        Agregar <i class='fas fa-shopping-cart'></i>
                                    </button>
                                </div>
                            </form>                            
<?php       
                        echo "</div>";
                    echo "</div>";
                echo "</div>";
            echo "</div>";
            echo "<br><br>";
            echo "<div class='row'>";
                echo "<div class='col-md-10'>";
                    echo "<a class='btn float-right login_btn3'";
                        echo "href=index.php?pid=" . base64_encode("presentacion/productos/consultarProductos.php") . "&m&categoria=" . $_GET["categoria"] . ">";
                        echo "ATRAS</a>";
                echo "</div>";
            echo "</div>";
        echo "</div>";
    echo "</section> ";
?>


<!--==========================
  Subscrbe Section
  ============================-->
<section id="subscribe">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-8">
				<h3 class="subscribe-title">GRACIAS POR VISITARNOS</h3>
				<p class="subscribe-text">Recuerda que estamos siempre a 
				tu servicio, dispuestos a brindarte los mas altos estandares
				de atencion y calidad en tus compras.</p>
			</div>
			<div class="col-md-4 subscribe-btn-container">
				<a class="subscribe-btn" href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>&pre">VUELVE PRONTO</a>
			</div>
		</div>
	</div>
</section>

<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All Rights
					Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>


<!--Este es el JQuery para cargar carrito: -->
<script>
    $(document).ready(function()
    {
        <?php
            echo "$(\"#cargarCarrito\").click(function()";
            echo "{\n";
            
            //Definimos la ruta en donde se recargara esta capa y 
            //la encriptamos respectivamente:        
            echo "url = \"indexAjax.php?pid=" . base64_encode("presentacion/productos/cargarCarritoAjax.php") . "\"\n";
            echo "$(\"#carrito\").load(url);\n";    
            echo "});\n\n";
        ?> 
    });
</script>
