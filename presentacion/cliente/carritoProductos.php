<?php
    //Primero obtengo el id del usuario que esta en la sesion:
    $idUsuario = $_SESSION["id"];
    
    //Luego busco el estado del pedido de ese usuario.
    //Para ello, realizo una consulta en la tabla de pedido:
    
    $pedido = new Pedido ("", "", "", "", $idUsuario);
    $pedido -> consultar(); 
    
    //Ahora verifico el estado del pedido:
    //1 = Seleccionado
    //2 = Solicitado
    
    if ($pedido -> getEstado() == 0)
    {
        //Quiere decir que no tiene ningun pedido activo y se debe crear uno nuevo:
        
        //Capturo fecha actual:
        $fecha_hora = "";
        $valor_total = 0;
        $estado = 1; //Es el estado por defecto   (Seleccionado)     
        
        //Creo el pedido con todos los atributos:
        $pedido = new Pedido ("", $fecha_hora, $valor_total, $estado, $idUsuario);
        $pedido -> crear();
        $pedido -> consultar(); //Consulto de nuevo para actualizar los pedidos
    }

    
    //Pero si el estado es 1 = SELECCIONADO entonces quiere decir que ya tiene 
    //un pedido activo y esta agregando productos a ese pedido.
    
  
    //Ahora se agregan productos al pedido:
    
    
    $idPedido = $pedido -> getIdPedido();
    $idProducto = $_GET["idPro"];
    
    if (isset($_POST["agregar"])) //Cuando va a la descripcion del producto y le da una cantidad
    {
        $cantidad_und = $_POST["cantidad"];
    }
    else
    {
        $cantidad_und = 1; //Esta agregando sin ver la descripcion
    }
    
    
    
    //Se verifica si quiere agregar un mismo producto en un mismo pedido:
    $carrito = new Carrito ($idPedido, $idProducto);
    
    
    if ($carrito -> consultar())
    {
        //Actualizo la cantidad_und de ese pedido, sumando la cantidad del 
        //producto que llega con la que tiene en la BD:
        
        $cantidad_und += $carrito -> getCantidad_und();
        $carrito = new Carrito ($idPedido, $idProducto, $cantidad_und);
        $carrito -> editar();
    }
    else 
    {
        //Creo el carrito con todos los atributos:
        $carrito = new Carrito ($idPedido, $idProducto, $cantidad_und);
        $carrito -> crear();
    }
?>

<!-- Para redireccionar a los productos y lanzar aviso: -->
<div id="preloader"></div>
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&agregado&categoria=<?php echo $_GET["categoria"]?>"/>
