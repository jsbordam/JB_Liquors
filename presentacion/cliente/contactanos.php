<!--==========================
  Contact Section
  ============================-->
<section id="contact">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h3 class="section-title">Contactanos</h3>
          <div class="section-title-divider"></div>
          <p class="section-description">Envianos un mensaje, tu opinion es muy importante para nosostros</p>
        </div>
      </div>
    
      <div class="row">
        <div class="col-md-3 col-md-push-2">
          <div class="info">
            <div>
              <i class="fa fa-map-marker"></i>
              <p>Carrera 195 #77-32<br>Bogota D.C</p>
            </div>
    
            <div>
              <i class="fa fa-envelope"></i>
              <p>JBliquorsCompany@gmail.com</p>
            </div>
    
            <div>
              <i class="fa fa-phone"></i>
              <p>+57 3013772681</p>
            </div>
    
          </div>
        </div>
    
        <div class="col-md-5 col-md-push-2">
          <div class="form">
            <div id="sendmessage">Tu mensaje ha sido enviado. Gracias!</div>
            <div id="errormessage"></div>
            <form action="" method="post" role="form" class="contactForm">
              <div class="form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Tu Nombre" data-rule="minlen:4" data-msg="Por favor ingrese al menos 4 caracteres" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Tu Correo" data-rule="email" data-msg="Ingresa un Correo Valido" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Por favor ingrese al menos 8 caracteres del asunto" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Por favor escribe algo para nosotros" placeholder="Mensaje"></textarea>
                <div class="validation"></div>
              </div>
              <div class="text-center"><button type="submit">Enviar mensaje</button></div>
            </form>
          </div>
        </div>   
      </div>
    </div>
</section>
  
  <!--==========================
  Subscrbe Section
  ============================-->
<section id="subscribe">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-8">
				<h3 class="subscribe-title">GRACIAS POR VISITARNOS</h3>
				<p class="subscribe-text">Recuerda que estamos siempre a tu
					servicio, dispuestos a brindarte los mas altos estandares de
					atencion y calidad en tus compras.</p>
			</div>
			<div class="col-md-4 subscribe-btn-container">
				<a class="subscribe-btn"
					href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>&pre">VUELVE
					PRONTO</a>
			</div>
		</div>
	</div>
</section>

<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  
