<?php
    //Como el pedido ya se realizo entonces cambia de estado,
    //Entonces busco el pedido con ese id y lo cambio a estado = 2 (SOLICITADO)
    //Y ademas le actualizo su valor total final por el cual se factura:
    
    //Fecha - Hora en que se solicita el pedido:
    $fecha_hora = date("Y/m/d - h:i:s");
    $pedido = new Pedido ($_GET["idPedido"], $fecha_hora, $_GET["valorTotal"]);
    $pedido -> editar();
?>
<section id="testimonials">
	<div class="container wow fadeInUp">
		<br><br><br>
    	<div class="card">
    		<div class="row">
    			<div class="col-md-12">		
    				<h1 class="text-center"><font face="Algerian" color="black" size="6px"><i class="fas fa-check-square"></i> T&Uacute; pedido se ha realizado con exito!</font></h1>
    				<br>
    				<h1 class="text-center"><font face="Arial Black" color="black" size="5px">La fiesta comenzar&aacute; pronto...</font></h1>				
    			</div>		
    		</div>	
    		<br>
    		<div class="row">
    			<div class="col-md-3">		
    				<img src="https://i.pinimg.com/originals/f0/58/14/f05814bdff3536c79f2fdea26ab42de6.gif" width="250px" height="250px">					
    			</div>		
    			<div class="col-md-3">			
    				<img src="https://i.pinimg.com/originals/7d/f1/dd/7df1ddeb78ab8211717cd74573de90c3.gif" width="250px" height="250px">							
    			</div>
    			<div class="col-md-3">			
    				<img src="https://i.pinimg.com/originals/60/ee/03/60ee032ae3bba927e30ed05fb4338b48.gif" width="250px" height="250px">							
    			</div>
    			
    			<div class="col-md-3">			
    				<img src="https://i.pinimg.com/originals/c0/15/35/c0153571e9dfe3649a90aa4fd9b040ef.gif" width="250px" height="250px">							
    			</div>			
    		</div>
    		
    		<br><br><br><br>
    		<div class="row">
    			<div class="col-md-12">			
    				<h3 class="text-center"><font face="Arial Black" color="black" size="2px">El exceso de alcohol es perjudicial para la salud.</font></h3>				
    			</div>	
    		</div>
    	</div>			
		<br><br><br><br>
	</div>
</section>

<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>

<meta http-equiv="refresh" content="4;url=index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/nuestrosProductos.php")?>&m"/>