<?php
    $cliente = new Usuario($_SESSION["id"]);
    $cliente -> consultar();
?>

<!--==========================
      Sesion Cliente:
      ============================-->
<?php include "presentacion/cliente/menuCliente.php"?>


<!--==========================
Testimonials Section
============================-->
<section id="testimonials">
	<div class="container wow fadeInUp">
		<div class="container wow fadeInUp">
			<div class="row">
				<div class="col-md-12">
					<h3 class="section-title-blanco">Bienvenido Cliente: <?php echo $cliente -> getNombre()?></h3>
					<div class="section-title-divider"></div>
					<h4 class="section-description">Siempre es un placer tenerte de
						vuelta!</h4>
				</div>
			</div>
		</div>

		<div class="container wow fadeInUp">
			<div class="row mt-5">
				<div class="col-md-6 text-center">
					<img src="img/cliente/1.jpg" width="100%" height="800px">
				</div>

				<div class="col-md-6 text-center">
					<img src="img/cliente/2.jpg" width="100%" height="800px">
				</div>
			</div>
		</div>
		<br>
		<div class="container wow fadeInUp">
			<div class="row">
				<div class="col-md-6 text-center">
					<img src="img/cliente/3.jpg" width="100%" height="800px">
				</div>

				<div class="col-md-6 text-center">
					<img src="img/cliente/4.jpg" width="100%" height="800px">
				</div>
			</div>
		</div>



	</div>
</section>


<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All Rights
					Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->