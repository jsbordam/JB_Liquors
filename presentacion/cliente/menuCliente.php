<?php 
    $cliente = new Usuario($_SESSION["id"]);
    $cliente -> consultar();
        
    
    //Primero busco el id del pedido de ese usuario.
    //Para ello, realizo una consulta en la tabla de pedido:
    
    //Para consultar solo el pedido que tenga estado = 1:
    $pedido = new Pedido ("", "", "", "", $cliente->getIdUsuario());
    $pedido -> consultar();
    
    
    //Para actualizar el numero de productos de un pedido que hay en el carrito:
    $carrito = new Carrito ($pedido->getIdPedido());
    $carrito -> sumarProductos();
?>

<!--==========================
      Secci�n de encabezado en menu
      ============================-->
<header id="header">
	<div class="container">
		<div class="pull-left">
			<h1>			
				<img src="<?php echo $cliente -> getFoto()?>" width="80px" height="60px">
				<font size= "5px" face="Algerian" COLOR="white"><?php echo $cliente -> getNombre() . " " . $cliente -> getApellido()?></font>
			</h1>
		</div>

		<!--Barra de navegacion: -->
	
		<nav id="nav-menu-container">					
			<ul class="nav-menu">
				<li class="menu-active"><a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/cliente/sesionCliente.php")?>"><font size="4"><i class="fas fa-home" data-toggle="tooltip" data-placement="bottom" title="Inicio"></i></font></a></li>
				
				<li><a href="#nav-menu-container">Nuestros Productos</a>
					<ul>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/nuestrosProductos.php")?>&m">Consultar disponibles</a></li>
					</ul>
				</li>
				
				<li>
					<a href="modalCarrito.php?idPedido=<?php echo $pedido -> getIdPedido()?>" data-toggle="modal" data-target="#modalCarrito">
						<div id="actualizarC">
							<font size="4">						
        						<i class="fas fa-shopping-cart" data-toggle="tooltip" data-placement="bottom" title="Mi Carrito">
        							<?php 
        							     echo $carrito->getCantidad_und();
        							?>
    							</i>
							</font>
						</div>
					</a>				
				</li>
				
				<li><a href="#nav-menu-container">Mi pedido</a>				
					<ul>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/factura.php")?>&m&idPedido=<?php echo $pedido -> getIdPedido()?>">Factura actual</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/historialPedidos.php")?>&m&idUsuario=<?php echo $cliente -> getIdUsuario()?>">Historial de pedidos</a></li>
					</ul>
				</li>
				
				<li><a href="#nav-menu-container">Contactanos</a>
					<ul>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/contactanos.php")?>&m">Enviar un mensaje</a></li>
					</ul>
				</li>
				
				<li><a href="index.php?pid=<?php echo base64_encode("presentacion/log.php")?>&m">Log</a></li>
				
				<li><a href="#nav-menu-container">Tu cuenta</a>
					<ul>
						<li><a href="#">Editar Perfil</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/editarFotoUsuario.php")?>&m&idUsu=<?php echo $_SESSION["id"]?>">Editar Foto</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/iniciarSesion.php")?>&session=0">Cerrar Sesion</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</header>


<!--Este es el Modal para carrito: -->
<section id="modalC">	
    <div class="modal fade" id="modalCarrito" tabindex="-1"
    	aria-labelledby="exampleModalLabel" aria-hidden="true"> 
    	<div class="modal-dialog">
    		<div class="modal-content" id="modal-content"></div>
    	</div>
    </div>
</section>    

<!--Este JavaScript es para el modal: -->
<script>
$('body').on('show.bs.modal', '.modal', function (e) {
	var link = $(e.relatedTarget);
	$(this).find(".modal-content").load(link.attr("href"));
});
</script>