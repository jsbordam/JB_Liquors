<section id="portfolio">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-12">
				<h3 class="section-title">Promociones</h3>
				<div class="section-title-divider"></div>
				<h4 class="section-description">Conoce algunas de nuestras
					promociones</h4>
			</div>
		</div>
	</div>

	<br>
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-3">
				<img
					src="https://verbenalicores.com/wp-content/uploads/elementor/thumbs/combo-4-or9yeajoe44dgzciaqqri5wspfshc1ecmv2rm3pk6o.png"
					width="300px" height="300px">
			</div>
			<div class="col-md-3">
				<img
					src="https://verbenalicores.com/wp-content/uploads/2020/06/combo-11-300x300.png"
					width="300px" height="300px">
			</div>
			<div class="col-md-3">
				<img
					src="https://verbenalicores.com/wp-content/uploads/2020/06/combo-13-300x300.png"
					width="300px" height="300px">
			</div>

			<div class="col-md-3">
				<img
					src="https://verbenalicores.com/wp-content/uploads/elementor/thumbs/combo-3-or9ye9lu7a335ddvg8c4xo5c41x44camaqfa4tqycw.png"
					width="300px" height="300px">
			</div>
		</div>
	</div>

	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-3">
				<img
					src="https://barranquilla.gruposotillo.com/wp-content/uploads/2017/11/IMG-20171102-WA0012-600x600.jpg"
					width="300px" height="300px">
			</div>
			<div class="col-md-3">
				<img
					src="https://barranquilla.gruposotillo.com/wp-content/uploads/2017/11/IMG-20171102-WA0017-600x600.jpg"
					width="300px" height="300px">
			</div>
			<div class="col-md-3">
				<img
					src="https://barranquilla.gruposotillo.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-06-at-11.36.48-AM-600x600.jpeg"
					width="300px" height="300px">
			</div>

			<div class="col-md-3">
				<img
					src="https://barranquilla.gruposotillo.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-31-at-11.08.39-AM-600x600.jpeg"
					width="300px" height="300px">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-3">
				<img
					src="https://barranquilla.gruposotillo.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-13-at-1.12.39-PM-600x600.jpeg"
					width="300px" height="300px">
			</div>
			<div class="col-md-3">
				<img
					src="https://barranquilla.gruposotillo.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-03-at-4.22.13-PM-600x600.jpeg"
					width="300px" height="300px">
			</div>
			<div class="col-md-3">
				<img
					src="https://barranquilla.gruposotillo.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-17-at-7.02.02-PM-600x600.jpeg"
					width="300px" height="300px">
			</div>

			<div class="col-md-3">
				<img src="https://pbs.twimg.com/media/CN18bvFWEAAEH3K.png"
					width="300px" height="300px">
			</div>
		</div>
	</div>
	<br>
</section>
<!--==========================
  Subscrbe Section
  ============================-->
<section id="subscribe">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-8">
				<h3 class="subscribe-title">GRACIAS POR VISITARNOS</h3>
				<p class="subscribe-text">Recuerda que estamos siempre a tu
					servicio, dispuestos a brindarte los mas altos estandares de
					atencion y calidad en tus compras.</p>
			</div>
			<div class="col-md-4 subscribe-btn-container">
				<a class="subscribe-btn"
					href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>&pre">VUELVE
					PRONTO</a>
			</div>
		</div>
	</div>
</section>

<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

