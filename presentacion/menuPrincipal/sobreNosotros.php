<section id="portfolio">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-12">
				<h3 class="section-title">Sobre Nosotros</h3>
				<div class="section-title-divider"></div>
				<h4 class="section-description">Aqui va info sobre nosotros</h4>
			</div>
		</div>
	</div>

	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-12">
				<p>TODO:</p>
			</div>
		</div>
	</div>
	<br><br><br><br><br><br><br><br><br>
</section> 

<!--==========================
  Subscrbe Section
  ============================-->
<section id="subscribe">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-8">
				<h3 class="subscribe-title">GRACIAS POR VISITARNOS</h3>
				<p class="subscribe-text">Recuerda que estamos siempre a 
				tu servicio, dispuestos a brindarte los mas altos estandares
				de atencion y calidad en tus compras.</p>
			</div>
			<div class="col-md-4 subscribe-btn-container">
				<a class="subscribe-btn" href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>&pre">VUELVE PRONTO</a>
			</div>
		</div>
	</div>
</section>
<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All Rights
					Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
