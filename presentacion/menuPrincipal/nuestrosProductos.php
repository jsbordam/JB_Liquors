<!--==========================
  Porfolio Section
  ============================-->
<section id="portfolio">

	<div class="container wow fadeInUp">
		<div class="row">
			<section id="slider2">
				<ul>
					<li><img src="https://ilvalle.com.co/website/wp-content/uploads/2020/10/precios-bajos-slider-desktop.jpg"></li>
					<li><img src="https://elamigodelanoche.com/wp-content/uploads/2018/10/El-amigo-de-la-noche-tienda-oficial-de-licores-junior-y-licores-la-amistad.jpg"></li>	
					<li><img src="https://www.mancholaagencia.com/wp-content/uploads/2020/10/digital_propio_3.jpg"></li>
					<li><img src="https://asifue.com.co/wp-content/uploads/2020/10/unnamed-1024x576.jpg"></li>										
				</ul>
			</section>			
		</div>
	
	</div>
 
	
	<br>
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-6">
				<a
					href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/nuestrosProductos.php")?>&m"
					class='btn float-left ver_btn'> VER POR CATALOGO </a>
			</div>
			<div class="col-md-6">
				<a
					href="modalProducto.php" data-toggle="modal" data-target="#modalProductos"
					class='btn float-right ver_btn'> VER POR FILTRO </a>
			</div>		
		</div>
		
		<br><br>
		
		<div class="row">
			<div class="col-md-12">
				<h3 class="section-title">Nuestros Productos</h3>
				<div class="section-title-divider"></div>
				<p class="section-description">Este es nuestro catalogo de licores</p>
			</div>
		</div>
	</div>

	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-4">
				<div class="card">
					<a class="portfolio-item"
						style="background-image: url(img/productos/aguardiente/aguardientes.jpg);"
						href="index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&categoria=1">
						<div class="details">
							<h4 class="card-title">
								<font face="Algerian">JB Liquors</font>
							</h4>
							<br> <br> <br>
							<h2 class="card-text">
								<font face="Arial Black">Ver mas</font>
							</h2>
							<h2 class="card-carrito">
								<i class="fas fa-search"></i>
							</h2>
						</div>
					</a>
					<div class="card-body">
						<h4 class="card-title">
							<font face="Algerian">Aguardiente</font>
						</h4>
						<h3 class="card-text">
							<font face="Arial">
								<?php 
								    //Para numero de productos en la categoria:
								    $producto = new Producto ("", "", "", "", "", "", 1);
								    echo "(" . $producto -> contarPro() . ")";
								?>
							</font>
						</h3>
					</div>
				</div>
			</div>
			
			
			<div class="col-md-4">
				<div class="card">
					<a class="portfolio-item"
						style="background-image: url(img/productos/brandy/brandys.jpg);"
						href="index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&categoria=2">
						<div class="details">
							<h4 class="card-title">
								<font face="Algerian">JB Liquors</font>
							</h4>
							<br> <br> <br>
							<h2 class="card-text">
								<font face="Arial Black">Ver mas</font>
							</h2>
							<h2 class="card-carrito">
								<i class="fas fa-search"></i>
							</h2>
						</div>
					</a>
					<div class="card-body">
						<h4 class="card-title">
							<font face="Algerian">Brandy</font>
						</h4>
						<h3 class="card-text">
							<font face="Arial">
								<?php 
								    //Para numero de productos en la categoria:
								    $producto = new Producto ("", "", "", "", "", "", 2);
								    echo "(" . $producto -> contarPro() . ")";
								?>
							</font>
						</h3>
					</div>
				</div>
			</div>		
			
			
			<div class="col-md-4">
				<div class="card">
					<a class="portfolio-item"
						style="background-image: url(img/productos/cerveza/cervezas.jpg);"
						href="index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&categoria=3">
						<div class="details">
							<h4 class="card-title">
								<font face="Algerian">JB Liquors</font>
							</h4>
							<br> <br> <br>
							<h2 class="card-text">
								<font face="Arial Black">Ver mas</font>
							</h2>
							<h2 class="card-carrito">
								<i class="fas fa-search"></i>
							</h2>
						</div>
					</a>
					<div class="card-body">
						<h4 class="card-title">
							<font face="Algerian">Cerveza</font>
						</h4>
						<h3 class="card-text">
							<font face="Arial">
								<?php 
								    //Para numero de productos en la categoria:
								    $producto = new Producto ("", "", "", "", "", "", 3);
								    echo "(" . $producto -> contarPro() . ")";
								?>
							</font>
						</h3>
					</div>
				</div>
			</div>				
		</div>
		
		<br><br>
		
		<div class="row">		
			<div class="col-md-4">
				<div class="card">
					<a class="portfolio-item"
						style="background-image: url(img/productos/champ/champ.jpg);"
						href="index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&categoria=4">
						<div class="details">
							<h4 class="card-title">
								<font face="Algerian">JB Liquors</font>
							</h4>
							<br> <br> <br>
							<h2 class="card-text">
								<font face="Arial Black">Ver mas</font>
							</h2>
							<h2 class="card-carrito">
								<i class="fas fa-search"></i>
							</h2>
						</div>
					</a>
					<div class="card-body">
						<h4 class="card-title">
							<font face="Algerian">Champa&ntilde;a</font>
						</h4>
						<h3 class="card-text">
							<font face="Arial">
								<?php 
								    //Para numero de productos en la categoria:
								    $producto = new Producto ("", "", "", "", "", "", 4);
								    echo "(" . $producto -> contarPro() . ")";
								?>
							</font>
						</h3>
					</div>
				</div>
			</div>
			
			
			<div class="col-md-4">
				<div class="card">
					<a class="portfolio-item"
						style="background-image: url(img/productos/ron/rones.jpg);"
						href="index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&categoria=5">
						<div class="details">
							<h4 class="card-title">
								<font face="Algerian">JB Liquors</font>
							</h4>
							<br> <br> <br>
							<h2 class="card-text">
								<font face="Arial Black">Ver mas</font>
							</h2>
							<h2 class="card-carrito">
								<i class="fas fa-search"></i>
							</h2>
						</div>
					</a>
					<div class="card-body">
						<h4 class="card-title">
							<font face="Algerian">Ron</font>
						</h4>
						<h3 class="card-text">
							<font face="Arial">
								<?php 
								    //Para numero de productos en la categoria:
								    $producto = new Producto ("", "", "", "", "", "", 5);
								    echo "(" . $producto -> contarPro() . ")";
								?>
							</font>
						</h3>
					</div>
				</div>
			</div>
			

			<div class="col-md-4">
				<div class="card">
					<a class="portfolio-item"
						style="background-image: url(img/productos/tequila/tequilas.jpg);"
						href="index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&categoria=6">
						<div class="details">
							<h4 class="card-title">
								<font face="Algerian">JB Liquors</font>
							</h4>
							<br> <br> <br>
							<h2 class="card-text">
								<font face="Arial Black">Ver mas</font>
							</h2>
							<h2 class="card-carrito">
								<i class="fas fa-search"></i>
							</h2>
						</div>
					</a>
					<div class="card-body">
						<h4 class="card-title">
							<font face="Algerian">Tequila</font>
						</h4>
						<h3 class="card-text">
							<font face="Arial">
								<?php 
								    //Para numero de productos en la categoria:
								    $producto = new Producto ("", "", "", "", "", "", 6);
								    echo "(" . $producto -> contarPro() . ")";
								?>
							</font>
						</h3>
					</div>
				</div>
			</div>		
		</div>
		
		<br><br>
		
		<div class="row">		
			<div class="col-md-4">
				<div class="card">
					<a class="portfolio-item"
						style="background-image: url(img/productos/vino/vinos.jpg);"
						href="index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&categoria=7">
						<div class="details">
							<h4 class="card-title">
								<font face="Algerian">JB Liquors</font>
							</h4>
							<br> <br> <br>
							<h2 class="card-text">
								<font face="Arial Black">Ver mas</font>
							</h2>
							<h2 class="card-carrito">
								<i class="fas fa-search"></i>
							</h2>
						</div>
					</a>
					<div class="card-body">
						<h4 class="card-title">
							<font face="Algerian">Vino</font>
						</h4>
						<h3 class="card-text">
							<font face="Arial">
								<?php 
								    //Para numero de productos en la categoria:
								    $producto = new Producto ("", "", "", "", "", "", 7);
								    echo "(" . $producto -> contarPro() . ")";
								?>
							</font>
						</h3>
					</div>
				</div>
			</div>		
			
			
			<div class="col-md-4">
				<div class="card">
					<a class="portfolio-item"
						style="background-image: url(img/productos/vodka/vodkas.jpg);"
						href="index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&categoria=8">
						<div class="details">
							<h4 class="card-title">
								<font face="Algerian">JB Liquors</font>
							</h4>
							<br> <br> <br>
							<h2 class="card-text">
								<font face="Arial Black">Ver mas</font>
							</h2>
							<h2 class="card-carrito">
								<i class="fas fa-search"></i>
							</h2>
						</div>
					</a>
					<div class="card-body">
						<h4 class="card-title">
							<font face="Algerian">Vodka</font>
						</h4>
						<h3 class="card-text">
							<font face="Arial">
								<?php 
								    //Para numero de productos en la categoria:
								    $producto = new Producto ("", "", "", "", "", "", 8);
								    echo "(" . $producto -> contarPro() . ")";
								?>
							</font>
						</h3>
					</div>
				</div>
			</div>


			<div class="col-md-4">
				<div class="card">
					<a class="portfolio-item"
						style="background-image: url(img/productos/whisky/whiskys.jpg);"
						href="index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&categoria=9">
						<div class="details">
							<h4 class="card-title">
								<font face="Algerian">JB Liquors</font>
							</h4>
							<br> <br> <br>
							<h2 class="card-text">
								<font face="Arial Black">Ver mas</font>
							</h2>
							<h2 class="card-carrito">
								<i class="fas fa-search"></i>
							</h2>
						</div>
					</a>
					<div class="card-body">
						<h4 class="card-title">
							<font face="Algerian">Whisky</font>
						</h4>
						<h3 class="card-text">
							<font face="Arial">
								<?php 
								    //Para numero de productos en la categoria:
								    $producto = new Producto ("", "", "", "", "", "", 9);
								    echo "(" . $producto -> contarPro() . ")";
								?>
							</font>
						</h3>
					</div>
				</div>
			</div>	
		</div>	
	</div>
</section>

<?php 
  //Si el rol de la sesion es (cliente): 
  if ($_SESSION["rol"] == "Cliente")
  {
?>
<!--==========================
  Subscrbe Section
  ============================-->
<section id="subscribe">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-8">
				<h3 class="subscribe-title">GRACIAS POR VISITARNOS</h3>
				<p class="subscribe-text">Recuerda que estamos siempre a tu
					servicio, dispuestos a brindarte los mas altos estandares de
					atencion y calidad en tus compras.</p>
			</div>
			<div class="col-md-4 subscribe-btn-container">
				<a class="subscribe-btn"
					href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>&pre">VUELVE
					PRONTO</a>
			</div>
		</div>
	</div>
</section>
<?php 
  }
?>


<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>


<!--Este es el Modal de Productos: -->

<section id="modalPro">	
    <div class="modal fade" id="modalProductos" tabindex="-1"
    	aria-labelledby="exampleModalLabel" aria-hidden="true"> 
    	<div class="modal-dialog">
    		<div class="modal-content" id="modal-content"></div>
    	</div>
    </div>
</section>  

<!--Este JavaScript es para el Modal de Productos: -->
<script>
$('body').on('show.bs.modal', '.modal', function (e) {
	var link = $(e.relatedTarget);
	$(this).find(".modal-content").load(link.attr("href"));
});
</script>
