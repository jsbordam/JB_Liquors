<?php 
    $error = 0;
    if (isset($_POST["actualizarFoto"]))
    {
       $foto = $_FILES["foto"];
       $tipo = $foto ["type"];
       
       if ($tipo == "image/jpeg" || $tipo == "image/png")
       {
           //Primero debo eliminar la foto anterior para cargar la nueva y que
           //as� no se llene de basura nuestro proyecto:
           
           $usuario = new Usuario ($_GET["idUsu"]);
           
           if ($usuario -> consultar() != NULL)
           {
               //Este metodo retira ese archivo que es el anterior para que
               //se almacene el nuevo atchivo de foto:
               unlink($usuario -> getFoto());              
           }
           
           
           //Necesito enviar los archivos de mi carpeta local (descargas)
           //a mi carpeta remota (imagenes) que esta dentro del servidor:
           $urlServidor = "fotosUsuarios/" . $usuario -> getNombre() . ".jpg"; 
           $urlLocal = $foto ["tmp_name"];// tmp_name es el nombre temporal que tiene ese archivo en el htdocs
           
           copy($urlLocal, $urlServidor); //Copie la imagen de local a servidor
           
           $usuario = new Usuario ($_GET["idUsu"], "", "", "", "", "", "", "", $urlServidor);
           $usuario -> editarFoto();
       }
       else 
       {
           $error = 1;
       }    
    }
    
    
    //Consulta foto de usuario:
    $usuario = new Usuario ($_GET["idUsu"]);
    $usuario -> consultar();
?>
<section id="portfolio">
	<div class="container wow fadeInUp">
		<div class="row">
			<div class="col-md-12">
				<h3 class="section-title">Editar Foto de Perfil</h3>
				<div class="section-title-divider"></div>
			</div>
		</div>
		<br><br>
		<div class="d-flex justify-content-center h-100">
			<div class="card2">
				<div class="card-body">
        			<div align="center"><img src="<?php echo $usuario->getFoto()?>" width="280px" height="280px"></div>										
        		</div>
				<div class="card-footer">
        				<?php
        				if(isset($_POST["actualizarFoto"]) && $error == 0)
                            { 
                        ?>                
                           		<section id="alertPro">          
                           		<div class="alert alert-dismissible fade show" role="alert">
                                    <strong><i class="fas fa-check-circle"></i>Foto actualizada correctamente!</strong> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    	<span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </section>  
                        <?php 
                            }
                            else if (isset($_POST["actualizarFoto"]) && $error == 1)
                            {
                        ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Error. El archivo debe ser de tipo: (jpg o png)</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                        
                        <?php 
                            }
                        ?>
					<form
						action="<?php echo "index.php?pid=" . base64_encode("presentacion/menuPrincipal/editarFotoUsuario.php") . "&m&idUsu=" . $_GET["idUsu"] . ""?>"
						method="post" enctype="multipart/form-data">
						<div class="form-group">
							<input type="file" name="foto" class="form-control-file" required="required">
						</div>
						<div class="form-group">
							<button type="submit" name="actualizarFoto" class="btn btn-primary btn-block">Actualizar Foto</button>
						</div>
					</form>
				</div>
			</div>	
		</div>
	</div>
	<br><br><br><br><br><br><br><br><br><br>
</section>
<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All Rights
					Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->