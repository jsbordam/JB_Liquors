<?php
    $error = 0;
    if (isset($_GET["error"])) 
    {
        $error = $_GET["error"];
    }
?>

<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>Iniciar Sesi&oacute;n</h3>
				<div class="d-flex justify-content-end social_icon">
					<a href="https://www.facebook.com/J.Sttiven.Borda.M/"><span><i class="fab fa-facebook-square"></i></span></a><span><i
						class="fab fa-google-plus-square"></i></span> <span><i
						class="fab fa-twitter-square"></i></span>
				</div>
			</div>
			<div class="card-body">
				<!-- Primero se valida si existe alguno de los 2 errores para dejar pasar -->
				<?php
                if ($error == 1) {
                    ?>                
           		<div class="alert alert-dismissible fade show" role="alert">
					<strong><i class="fas fa-exclamation-triangle"></i> Correo o clave incorrectos. Vuelve a intentarlo</strong>
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
                <?php
                } else if ($error == 2) {
                    ?>
            	<div class="alert alert-dismissible fade show" role="alert">
					<strong><i class="fas fa-exclamation-triangle"></i> Usuario deshabilitado. No puedes ingresar.</strong>
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
                <?php }?>              
				
				<form
					action=<?php echo "index.php?pid=" . base64_encode("presentacion/autenticar.php")?>
					method="post">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="email" name="correo" class="form-control"
							placeholder="Correo" required="required">
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" name="clave" class="form-control"
							placeholder="Clave" required="required"> <br>
						<br> <a href="#">Olvidaste tu clave?</a>
					</div>

					<div class="form-group">
						<input type="submit" value="Entrar"
							class="btn float-right login_btn btn-block">
					</div>
					<div class="form-group">
						<a class="btn float-right login_btn2"
							href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>">
							VOLVER A LA TIENDA</a>
					</div>
				</form>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					<a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/registrarse.php")?>">Crear cuenta</a>
				</div>
			</div>
		</div>
	</div>
</div>