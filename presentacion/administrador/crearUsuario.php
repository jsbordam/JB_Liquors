<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Administrador") //Funcion solo para admi
    {
?>
<section id="admin">
	<div class="container wow fadeInUp">			
		<div class="d-flex justify-content-center h-100">
			<div class="card">
				<div class="card-header">
					<?php 
					    if (isset($_GET["cliente"])) //Esta creando un cliente
					    {	
					        $usu = 2;
					?>		
					<h3 class="titulo">Creando Cliente</h3>
					<br><br>
					<div align="center"><img src="https://fscomps.fotosearch.com/compc/CSP/CSP684/drinking-from-the-bottle-man-clip-art__k6840046.jpg" width="280px" height="280px"></div>							
					<?php 
					    }
					    else if (isset($_GET["proveedor"]))
					    {
					        $usu = 3;
					?>
					<h3 class="titulo">Creando Proveedor</h3>
					<br><br>
					<div align="center"><img src="https://comps.canstockphoto.es/barman-botellas-barra-clip-art-vectorial_csp27257710.jpg" width="280px" height="280px"></div>					
					<?php 
					    }
					    else
					    {
					        $usu = 1;
					?>
					<h3 class="titulo">Creando Administrador</h3>
					<br><br>
					<div align="center"><img src="https://media.istockphoto.com/vectors/german-man-drinking-beer-from-a-stein-vector-id1003178252" width="280px" height="280px"></div>					
					<?php 
					    }
					?>	
					
				</div>
				<br>
				<div class="card-body">
					<form
						action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/consultarUsuario.php") . "&m&crear&rol=" . $usu . ""?>
						method="post" enctype="multipart/form-data">

					<div class="form-row">
						<div class="form-group col-sm-4">
							<input type="text" name="nombre"
								class="form-control" placeholder="Nombre"
								required="required">
						</div>
						<div class="form-group col-sm-4">
							<input type="text" name="apellido"
								class="form-control" placeholder="Apellido"
								required="required">
						</div>
						<div class="form-group col-sm-4">
							<select id="inputState" class="form-control" name="tipo_docto">
								<option>Cedula Ciudadania</option>
								<option>Tarjeta de Identidad</option>
								<option>Carnet de Extranjeria</option>					
							</select>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-group col-sm-4">
							<input type="text" name="num_docto"
								class="form-control" placeholder="Numero de Documento"
								required="required">
						</div>
						<div class="form-group col-sm-4">
							<input type="email" name="correo"
								class="form-control" placeholder="Correo"
								required="required">
						</div>
						<div class="form-group col-sm-4">
							<input type="password" name="clave"
								class="form-control" placeholder="Clave"
								required="required">
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-group col-sm-4">
							<select id="inputState" class="form-control" name="estado">
								<option>Habilitado</option>
								<option>Deshabilitado</option>				
							</select>
						</div>
						<div class="form-group col-sm-4">
							<label>Foto:</label>
							<div class="form-group">
								<input type="file" name="foto" class="form-control-file"
									required="required">
							</div>
						</div>
						<div class="form-group col-sm-4">
							<select id="inputState" class="form-control" name="idRol">
								<?php 
								    if (isset($_GET["cliente"])) //Esta creando un cliente
								    {	
								?>							
								<option>Cliente</option>	
								<?php 
								    }
								    else if (isset($_GET["proveedor"])) //Esta creando un proveedor
								    {	
								?>	
								<option>Proveedor</option>
								<?php 
								    }
								    else //Esta creando un Administrador 
								    {
								?>	
								<option>Administrador</option>	
								<?php 
								    }
								?>					
							</select>
						</div>
					</div>
					
					<br>
					
					<div class="form-group">
						<input type="submit" value="Crear"
							class="btn float-right login_btn btn-block">
					</div>				
				</form>
				</div>								
			</div>
		</div>
	</div>
	<br><br><br><br><br><br><br><br><br><br>
</section>
<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All Rights
					Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->

<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"/>
<?php
}
?>