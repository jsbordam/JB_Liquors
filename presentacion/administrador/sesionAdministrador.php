<?php
    $administrador = new Usuario($_SESSION["id"]);
    $administrador -> consultar();
?>

<!--==========================
      Sesion Administrador:
      ============================-->
<?php include "presentacion/administrador/menuAdministrador.php"?>


 <!--==========================
  Testimonials Section
  ============================-->
<section id="admin">
	<div class="container wow fadeInUp">
		<div class="container wow fadeInUp">
			<div class="row">
				<div class="col-md-12">
					<h3 class="section-title-blanco">Bienvenido Administrador: <?php echo $administrador -> getNombre()?></h3>
					<div class="section-title-divider"></div>
					<h4 class="section-description">Siempre es un placer tenerte de
						vuelta!</h4>
				</div>
			</div>
		</div>

		<div class="container wow fadeInUp">
			<div class="row mt-5">
				<div class="col-md-12 text-center">
					<img src="https://i.pinimg.com/564x/6d/00/bb/6d00bb2481ff3d59fac8ae6d3ad70d0c.jpg" width="500px" height="560px">
				</div>


			</div>
		</div>
		<br>
	</div>
</section>


<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All Rights
					Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->