<?php
if (isset($_SESSION["id"]))
{
    if ($_SESSION["rol"] == "Administrador") //Funcion solo para admi
    {
?>
<?php
    //Primero identifico usuario quiere consultar o crear:
    //1 - Administrador
    //2 - Cliente
    //3 - Proveedor
    $rol = $_GET["rol"];
    
    
    //Cuando quiere crear un usuario:
    if (isset($_GET["crear"]))
    {
        //Recojo los datos que llegan por POST del formulario:
        $nombre = $_POST["nombre"];
        $apellido = $_POST["apellido"];
        
        //Para opcion tipo_docto:
        if ($_POST["tipo_docto"] == "Cedula Ciudadania")
        {
            $tipo_docto = 0;
        }
        else if ($_POST["tipo_docto"] == "Tarjeta de Identidad")
        {
            $tipo_docto = 1;
        }
        else 
        {
            $tipo_docto = 2; //Carnet de Extranjeria
        }
        
        $num_docto = $_POST["num_docto"];
        $correo = $_POST["correo"];
        $clave = md5 ($_POST["clave"]); //Encriptamos la clave con MD5
        
        //Para opcion estado:
        if ($_POST["estado"] == "Habilitado")
        {
            $estado = 1;
        }
        else 
        {
            $estado = 0; //Deshabilitado
        }
        
        
        //////////////////////////////////////////////////////////////////////////////////
        //Para el archivo de foto:
        
        $foto = $_FILES["foto"];
        $tipo = $foto ["type"];
        
        if ($tipo == "image/jpeg" || $tipo == "image/png")
        {
            
            //Esta sera la direccion final que tendra la foto para que se guarde en la carpeta
            //del proyecto:
            
            //El nombre de la foto sera el nombre del usuario:
            
            $urlServidor = "fotosUsuarios/" . $nombre. ".jpg";
            $urlLocal = $foto ["tmp_name"];// tmp_name es el nombre temporal que tiene ese archivo en el htdocs
            
            copy($urlLocal, $urlServidor); //Copie la imagen de local a servidor
        }
        else
        {
            $urlServidor = "Formato de foto incorrecto";
        }
        
        //////////////////////////////////////////////////////////////////////////////////
        

        $idRol = $rol; //Numero del rol del usuario que se crear�
        
        
        //Creo el usuario:
        $usuario = new Usuario ("", $nombre, $apellido, $tipo_docto, $num_docto, $correo, $clave, $estado, $urlServidor, $idRol);
        $usuario -> crear();   
    }
    
    //Quiere consultar una lista de usuarios:
    $usuario = new Usuario("", "", "", "", "", "", "", "", "", $rol);
    $usuarios = $usuario -> consultarTodos(); //Arreglo de Usuarios con rol: (1/2/3)
?>


<section id="admin">
	<div class="container wow fadeInUp">
		<?php 
		  if ($rol == 2) //Esta consultando un cliente
		    {	
		?>	
		<h3 class="section-title-blanco">Clientes registrados en el sistema</h3>
		<?php 
		    }
		    else if ($rol == 3) //Esta consultando un proveedor
		    {
		?>
		<h3 class="section-title-blanco">Proveedores registrados en el sistema</h3>
		<?php 
		    }
		    else //Esta consultando un admin
		    {
		?>
		<h3 class="section-title-blanco">Administradores registrados en el sistema</h3>
		<?php 
		    }
		?>
		
		<div class="section-title-divider"></div>
		<br><br>
		
		<!-- Para avisar cuando se crea un usuario -->
        <?php
            if(isset($_GET["crear"]))
            { 
        ?>  
            <section id="alertPro">          
           		<div class="alert alert-dismissible fade show" role="alert">
           		
           			<?php 
           			      if ($rol == 1)
           			      {        
           			?>
                    <strong><i class="fas fa-check-circle"></i> Genial: Has creado un nuevo Administrador correctamente!</strong> 
                    <?php 
                          }
                          else if ($rol == 2)
                          {                          
                    ?>
                    <strong><i class="fas fa-check-circle"></i> Genial: Has creado un nuevo Cliente correctamente!</strong> 
                    <?php 
                          }
                          else
                          {                          
                    ?>
                    <strong><i class="fas fa-check-circle"></i> Genial: Has creado un nuevo Proveedor correctamente!</strong> 
                    <?php 
                          }                        
                    ?>
                    
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    	<span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </section>          
        <?php 
            }
        ?>
        
		<table class="table table-info table-striped table-hover">
			<thead>
				<tr>
					<th class="text-center"><h5><font face='Arial Black' size='3' color='black'>Codigo</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='3' color='black'>Nombre</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='3' color='black'>Apellido</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='3' color='black'>Tipo Documento</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='3' color='black'>Num Documento</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='3' color='black'>Correo</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='3' color='black'>Estado</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='3' color='black'>Foto</font></h5></th>	
					<th class="text-center"><h5><font face='Arial Black' size='3' color='black'>Acciones</font></h5></th>				
				</tr>
			</thead>
			<tbody>
            	<?php 
            	    foreach ($usuarios as $usuarioActual)
                	{                  	   
                	    echo "<tr>";
                    	    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getIdUsuario() . "</font></h5></td>";             	        
                    	    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getNombre() . "</font></h5></td>";
                    	    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getApellido() . "</font></h5></td>";
                    	    
                    	    //Para tipo de documento:
                    	    if ($usuarioActual->getTipo_docto() == 0)
                    	    {
                    	        $tipoDoc = "Cedula Ciudadania";
                    	    }
                    	    else if ($usuarioActual->getTipo_docto() == 1)
                    	    {
                    	        $tipoDoc = "Tarjeta de Identidad";
                    	    }
                    	    else 
                    	    {
                    	        $tipoDoc = "Carnet de Extranjeria";
                    	    }
                    	      
                    	    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $tipoDoc . "</font></h5></td>";
                    	    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getNum_docto() . "</font></h5></td>";
                    	    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getCorreo() . "</font></h5></td>";
                    	    echo "<td class='text-center'><h5><font face='Arial' size='5' color='black'><div id='estado" . $usuarioActual -> getIdUsuario() . "'>" . (($usuarioActual -> getEstado() == 1)?"<i class='fas fa-user-check' data-toggle='tooltip' data-placement='bottom' title='Habilitado'></i>":"<i class='fas fa-user-times' data-toggle='tooltip' data-placement='bottom' title='Deshabilitado'></i>") . "</font></h5></td>";
                    	    echo "<td class='text-center'><a href='modalFoto.php?idUsu=". $usuarioActual -> getIdUsuario() ."' data-toggle='modal' data-target='#modalFoto'><font face='Arial' size='5' color='red'><i class='fas fa-eye' data-toggle='tooltip' data-placement='bottom' title='Ver Foto'></i></font></a></td>"; 
                    	    echo "<td class='text-center'><a href='modalAcciones.php?rol=" . $rol . "&idUsu=" . $usuarioActual -> getIdUsuario() . "' data-toggle='modal' data-target='#modalAcciones'><font face='Arial' size='5' color='black'><i class='far fa-folder-open' data-toggle='tooltip' data-placement='bottom' title='Abrir'></i></font></a></td>";
                	    echo "</tr>";                	   
                	}              	
            	?>
    	</tbody>
	</table>
	<br>
	<div class="row">
		<div class="col-md-10">		
    		<a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarUsuario.php")?>&m&rol=<?php echo $rol?>" class='btn float-right login_btn3'>
    			ACTUALIZAR
    		</a>
    	</div>
	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br>			
	</div>
</section>



<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>	


<!--Este es el Modal de Acciones: -->

<section id="modalA">	
    <div class="modal fade" id="modalAcciones" tabindex="-1"
    	aria-labelledby="exampleModalLabel" aria-hidden="true"> 
    	<div class="modal-dialog">
    		<div class="modal-content" id="modal-content"></div>
    	</div>
    </div>
</section>  

<!--Este JavaScript es para el Modal de Acciones: -->
<script>
$('body').on('show.bs.modal', '.modal', function (e) {
	var link = $(e.relatedTarget);
	$(this).find(".modal-content").load(link.attr("href"));
});
</script>


<!--Este es el Modal de Ver Foto: -->

<section id="modalF">	
    <div class="modal fade" id="modalFoto" tabindex="-1"
    	aria-labelledby="exampleModalLabel" aria-hidden="true"> 
    	<div class="modal-dialog">
    		<div class="modal-content" id="modal-content"></div>
    	</div>
    </div>
</section>  

<!--Este JavaScript es para el Modal de Acciones: -->
<script>
$('body').on('show.bs.modal', '.modal', function (e) {
	var link = $(e.relatedTarget);
	$(this).find(".modal-content").load(link.attr("href"));
});
</script>

<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"/>
<?php
}
?>