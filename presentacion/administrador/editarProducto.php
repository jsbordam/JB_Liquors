<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Administrador") //Funcion solo para admi
    {
?>
<?php   
    //Para consultar los datos del producto que quiere editar:
    $producto = new Producto ($_GET["idPro"]);
    $producto -> consultar();
?>


<section id="admin">
	<div class="container wow fadeInUp">	
		<div class="d-flex justify-content-center h-100">
    		<div class="card2">
        		<div class="card-body">
        			<div align="center"><img src="<?php echo $producto->getFoto()?>" width="280px" height="280px"></div>										
        		</div>
    		</div>
    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<div class="card2">
				<div class="card-header">
					<h3 class="titulo">Editando Producto #<?php echo $_GET["idPro"]?></h3>	
				</div>
				<div class="card-body">
					<form
						action=<?php echo "index.php?pid=" . base64_encode("presentacion/productos/consultarProductos.php") . "&m&editar&categoria=" . $_GET["categoria"] . "&idPro=" . $_GET["idPro"] . ""?>
						method="post">

					<div class="form-row">
						<div class="form-group col-sm-4">
							<label>Nombre:</label> <input type="text" name="nombre"
								class="form-control" placeholder="Nuevo nombre" value="<?php echo $producto->getNombre()?>"
								required="required">
						</div>
						<div class="form-group col-sm-4">
							<label>Descripcion:</label> <input type="text" name="descripcion"
								class="form-control" placeholder="Nueva Descripcion" value="<?php echo $producto->getDescripcion()?>"
								required="required">
						</div>
						<div class="form-group col-sm-4">
							<label>Foto:</label> <input type="text" name="foto"
								class="form-control" placeholder="Nueva Url Foto" value="<?php echo $producto->getFoto()?>"
								required="required">
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-group col-sm-4">
							<label>Unidades Disponibles:</label> <input type="text" name="und_dis"
								class="form-control" placeholder="Nuevas Unidades Dispo" value="<?php echo $producto->getUnd_dis()?>"
								required="required">
						</div>
						<div class="form-group col-sm-4">
							<label>Valor:</label> <input type="text" name="valor"
								class="form-control" placeholder="Nuevo Valor" value="<?php echo $producto->getValor()?>"
								required="required">
						</div>
						<div class="form-group col-sm-4">
							<label for="inputState">Categoria:</label> <select
								id="inputState" class="form-control" name="categoria">
								<option>Aguardiente</option>
								<option>Brandy</option>
								<option>Cerveza</option>
								<option>Champa&ntilde;a</option>
								<option>Ron</option>
								<option>Tequila</option>
								<option>Vino</option>
								<option>Vodka</option>
								<option>Whisky</option>						
							</select>
						</div>
					</div>
					
					<br>
					
					<div class="form-group">
						<input type="submit" value="Guardar"
							class="btn float-right login_btn btn-block">
					</div>				
					
					<div class="form-group">
						<a class="btn float-right login_btn3"
							href="index.php?pid=<?php echo base64_encode("presentacion/productos/consultarProductos.php")?>&m&categoria=<?php echo $_GET["categoria"]?>">
							ATRAS</a>
					</div>
				</form>
				</div>								
			</div>
		</div>
	</div>
	<br><br><br><br><br><br><br><br><br><br>
</section>
<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All Rights
					Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->
<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"/>
<?php
}
?>