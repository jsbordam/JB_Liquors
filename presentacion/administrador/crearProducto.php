<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Administrador" || $_SESSION["rol"] == "Proveedor") //Funcion solo para admi
    {
?>
<?php   
    //Para crear producto:
    if (isset($_POST["crear"]))
    {
        $error=0;
        //Estos son los datos que llegan por el formulario cuando se quiere editar:
        $nombre = $_POST["nombre"];
        $descripcion = $_POST["descripcion"];
        $und_dis = $_POST["und_dis"];
        $valor = $_POST["valor"];
        
        //Para categoria:
        if ($_POST["categoria"] == "Aguardiente")
        {
            $categoria = 1;
        }
        else if ($_POST["categoria"] == "Brandy")
        {
            $categoria = 2;
        }
        else if ($_POST["categoria"] == "Cerveza")
        {
            $categoria = 3;
        }
        else if ($_POST["categoria"] == "Ron")
        {
            $categoria = 5;
        }
        else if ($_POST["categoria"] == "Tequila")
        {
            $categoria = 6;
        }
        else if ($_POST["categoria"] == "Vino")
        {
            $categoria = 7;
        }
        else if ($_POST["categoria"] == "Vodka")
        {
            $categoria = 8;
        }
        else if ($_POST["categoria"] == "Whisky")
        {
            $categoria = 9;
        }
        else
        {
            $categoria = 4; //Para categoria CHAMPA�A
        }
               
        //////////////////////////////////////////////////////////////////////////////////
        //Para el archivo de foto:
        
        $foto = $_FILES["foto"];
        $tipo = $foto ["type"];
        
        if ($tipo == "image/jpeg" || $tipo == "image/png")
        {
            
            //Esta sera la direccion final que tendra la foto para que se guarde en la carpeta
            //del proyecto:
            
            //Se le da nombre con time() para que guarde la fecha y la hora excata en que cambia su foto
            //De esta manera se controla que no hayan fotos con el mismo nombre:
            
            $urlServidor = "img/productos/nuevos/" . time() . ".jpg";
            $urlLocal = $foto ["tmp_name"];// tmp_name es el nombre temporal que tiene ese archivo en el htdocs
            
            copy($urlLocal, $urlServidor); //Copie la imagen de local a servidor
        }
        else
        {
            $error=1;
        }
        
        //////////////////////////////////////////////////////////////////////////////////
        
        
        if ($error == 0) //Si no hay error en el formato de la foto entonces pasa
        {
            //Envio esos datos que tendra el producto:
            $producto = new Producto ("", $nombre, $descripcion, $urlServidor, $und_dis, $valor, $categoria);
            $producto -> crear();
        }     
    }
?>


<section id="admin">
	<div class="container wow fadeInUp">	
		<div class="d-flex justify-content-center h-100">
			<div class="card2">
				<div class="card-header">
					<h3 class="titulo">Creando Producto</h3>
					<br>
					<div align="center"><img src="https://licoresdelasabana.com/wp-content/uploads/2019/08/whiski_licores_de_la_sabana-min.jpg" width="280px" height="280px"></div>	
				</div>
				<br>
				<div class="card-body">
					<!-- Para avisar cuando se crea un producto -->
                        <?php
                            if(isset($_POST["crear"]) && $error == 0)
                            { 
                        ?>  
                            <section id="alertPro">          
                           		<div class="alert alert-dismissible fade show" role="alert">
                                    <strong><i class="fas fa-check-circle"></i> Genial: Producto creado correctamente!</strong> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    	<span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </section>                  
                        <?php 
                            }
                        ?>
				
						<!-- Para reporatar error en el archivo de foto-->
        				<?php
        				    if (isset($_POST["crear"]) && $error == 1)
                            { 
                        ?>                
                           		<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Error. El archivo foto debe ser de tipo: (jpg o png)</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                        <?php 
                            }               
                        ?>
					<form
						action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/crearProducto.php") . "&m"?>
						method="post" enctype="multipart/form-data">

					<div class="form-row">
						<div class="form-group col-sm-4">
							<input type="text" name="nombre"
								class="form-control" placeholder="Nombre"
								required="required">
						</div>
						<div class="form-group col-sm-4">
							<input type="text" name="descripcion"
								class="form-control" placeholder="Descripcion"
								required="required">
						</div>						
						<div class="form-group col-sm-4">
							<input type="number" name="und_dis"
								class="form-control" placeholder="Unidades Disponibles"
								required="required">
						</div>
					</div>
					
					<div class="form-row">						
						<div class="form-group col-sm-4">
							<input type="number" name="valor"
								class="form-control" placeholder="Valor" 
								required="required">
						</div>
						<div class="form-group col-sm-4">
							<select id="inputState" class="form-control" name="categoria">
								<option>Aguardiente</option>
								<option>Brandy</option>
								<option>Cerveza</option>
								<option>Champa&ntilde;a</option>
								<option>Ron</option>
								<option>Tequila</option>
								<option>Vino</option>
								<option>Vodka</option>
								<option>Whisky</option>						
							</select>
						</div>
						<div class="form-group col-sm-4">
							<label>Foto:</label>
							<div class="form-group">
								<input type="file" name="foto" class="form-control-file"
									required="required">
							</div>
						</div>
					</div>
					
					<br>
					
					<div class="form-group">
						<input type="submit" name="crear" value="Crear"
							class="btn float-right login_btn btn-block">
					</div>				
				</form>
				</div>								
			</div>
		</div>
	</div>
	<br><br><br><br><br><br><br><br><br><br>
</section>
<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All Rights
					Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->
<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"/>
<?php
}
?>