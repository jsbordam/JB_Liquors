<?php
    $pedido = new Pedido ();
    //Me trae un array de todos los registros de pedido
    $arregloPedidos = $pedido -> consultarTodos2();
?>

<section id="portfolio">
	<div class="container wow fadeInUp">
		<h3 class="section-title">Pedidos</h3>
		<div class="section-title-divider"></div>
		<br><br>
		<table class="table table-info table-striped table-hover">
			<thead>
				<tr>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Codigo</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Fecha - Hora</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Valor Total</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Productos</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Estado</font></h5></th>
					<th class="text-center"><h5><font face='Arial Black' size='4' color='black'>Id Cliente</font></h5></th>
				</tr>
			</thead>
			<tbody>
            	<?php 
                    foreach ($arregloPedidos as $pedidoActual)
                	{   
                	    //Busco el carrito de productos que tiene registrado ese idPedido
                	    $carrito = new Carrito ($pedidoActual->getIdPedido());
                	    
                	    //Me trae un array de todos los registros asociados a ese pedido
                	    $arregloCarrito = $carrito -> consultarTodos(); 
                	    
                	    $unidadesTotal=0;
                	    //Para calcular cuantos productos pidio en ese pedido
                	    foreach ($arregloCarrito as $carritoActual)
                	    {
                	        $unidadesTotal += $carritoActual -> getCantidad_und();
                	    }
                	    
                	    //Muestro los datos del pedido que necesito:
                	    echo "<tr>";
                	       echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getIdPedido() . "</font></h5></td>";             	        
                	       echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getFecha_hora() . "</font></h5></td>";
                	       echo "<td class='text-center'><font face='Arial' size='3' color='black'>$" . number_format($pedidoActual->getValor_total(), ...array(0, ',', '.')) . "</font></td>";               	        
                	       echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $unidadesTotal . " Unidades" . "</font></h5></td>";
                	       
                	       if ($pedidoActual->getEstado() == 1)//Seleccionando
                	       {
                	           echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>SELECCIONANDO</font></h5></td>";
                	       }
                	       else//Solicitado 
                	       {
                	           echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>SOLICITADO</font></h5></td>";
                	       } 
                	       echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getIdUsuario() . "</font></h5></td>";
                	   echo "</tr>";                	   
                	}              	
            	?>
    	</tbody>
	</table>
	<br>
	<div class="row">
		<div class="col-md-10">		
    		<a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarPedidos.php")?>&m" class='btn float-right login_btn3'>
    			ACTUALIZAR
    		</a>
    	</div>
	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br>			
	</div>
</section>

<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>	
