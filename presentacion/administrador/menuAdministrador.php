<?php 
    $administrador = new Usuario($_SESSION["id"]);
    $administrador -> consultar();
?>

<!--==========================
      Secci�n de encabezado en menu
      ============================-->
<header id="header">
	<div class="container">
		<div class="pull-left">
			<h1>			
				<img src="<?php echo $administrador -> getFoto()?>" width="80px" height="60px">
				<font size= "5px" face="Algerian" COLOR="white"><?php echo $administrador -> getNombre() . " " . $administrador -> getApellido()?></font>
			</h1>
		</div>

		<!--Barra de navegacion: -->
	
		<nav id="nav-menu-container">					
			<ul class="nav-menu">
				<li class="menu-active"><a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/sesionAdministrador.php")?>"><font size="4"><i class="fas fa-home" data-toggle="tooltip" data-placement="bottom" title="Inicio"></i></font></a></li>
				
				<li><a href="#nav-menu-container">Productos</a>
					<ul>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/crearProducto.php")?>&m">Crear</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/nuestrosProductos.php")?>&m">Consultar</a></li>
					</ul>
				</li>
				<li><a href="#nav-menu-container">Clientes</a>
					<ul>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/crearUsuario.php")?>&m&cliente">Crear</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarUsuario.php")?>&m&rol=2">Consultar</a></li>
					</ul>
				</li>
				<li><a href="#nav-menu-container">Proveedores</a>
					<ul>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/crearUsuario.php")?>&m&proveedor">Crear</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarUsuario.php")?>&m&rol=3">Consultar</a></li>
					</ul>
				</li>
				<li><a href="#nav-menu-container">Administradores</a>
					<ul>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/crearUsuario.php")?>&m&admin">Crear</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarUsuario.php")?>&m&rol=1">Consultar</a></li>
					</ul>
				</li>
				<li><a href="#nav-menu-container">Pedidos</a>
					<ul>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarPedidos.php")?>&m">Consultar</a></li>
					</ul>
				</li>
				<li><a href="index.php?pid=<?php echo base64_encode("presentacion/log.php")?>&m">Log</a></li>
				
				<li><a href="#nav-menu-container">Tu cuenta</a>
					<ul>
						<li><a href="#">Editar Perfil</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/editarFotoUsuario.php")?>&m&idUsu=<?php echo $_SESSION["id"]?>">Editar Foto</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/iniciarSesion.php")?>&session=0">Cerrar Sesion</a></li>
					</ul>
				</li>
			</ul>
		</nav>	
	</div>
</header>