<?php 
    $proveedor = new Usuario($_SESSION["id"]);
    $proveedor -> consultar();
?>

<!--==========================
      Secci�n de encabezado en menu
      ============================-->
<header id="header">
	<div class="container">
		<div class="pull-left">
			<h1>			
				<img src="<?php echo $proveedor -> getFoto()?>" width="80px" height="60px">
				<font size= "5px" face="Algerian" COLOR="white"><?php echo $proveedor -> getNombre() . " " . $proveedor -> getApellido()?></font>
			</h1>
		</div>

		<!--Barra de navegacion: -->
	
		<nav id="nav-menu-container">					
			<ul class="nav-menu">
				<li class="menu-active"><a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/sesionProveedor.php")?>"><font size="4"><i class="fas fa-home" data-toggle="tooltip" data-placement="bottom" title="Inicio"></i></font></a></li>
				
				<li><a href="#nav-menu-container">Productos</a>
					<ul>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/crearProducto.php")?>&m">Registrar</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/nuestrosProductos.php")?>&m">Consultar listado</a></li>
					</ul>
				</li>				
				<li><a href="#nav-menu-container">Contactanos</a>
					<ul>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/contactanos.php")?>&m">Enviar un mensaje</a></li>
					</ul>
				</li>
				<li><a href="index.php?pid=<?php echo base64_encode("presentacion/log.php")?>&m">Log</a></li>
				
				<li><a href="#nav-menu-container">Tu cuenta</a>
					<ul>
						<li><a href="#">Editar Perfil</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/editarFotoUsuario.php")?>&m&idUsu=<?php echo $_SESSION["id"]?>">Editar Foto</a></li>
						<li><a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/iniciarSesion.php")?>&session=0">Cerrar Sesion</a></li>
					</ul>
				</li>
			</ul>
		</nav>	
	</div>
</header>