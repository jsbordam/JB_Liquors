<?php
    $proveedor = new Usuario($_SESSION["id"]);
    $proveedor -> consultar();
?>

<!--==========================
      Sesion Cliente:
      ============================-->
<?php include "presentacion/proveedor/menuProveedor.php"?>


 <!--==========================
  Testimonials Section
  ============================-->
  <section id="testimonials">
    <div class="container wow fadeInUp">
		<div class="container wow fadeInUp">
			<div class="row">
				<div class="col-md-12">
					<h3 class="section-title-blanco">Bienvenido Proveedor: <?php echo $proveedor -> getNombre()?></h3>
					<div class="section-title-divider"></div>
					<h4 class="section-description">Siempre es un placer tenerte de
						vuelta!</h4>
				</div>
			</div>
		</div>

		<div class="container wow fadeInUp">
			<div class="row mt-5">
				<div class="col-md-12 text-center">
					<img src="https://uploads-ssl.webflow.com/575ef60509a5a7a9116d9f8c/5de6e2342578f7cf997f6e22_proveedor.png" width="800px" height="560px">
				</div>


			</div>
		</div>
		<br>
	</div>
  </section>


<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All Rights
					Reserved
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- #footer -->