<section id="portfolio">
	<div class="container wow fadeInUp">		
		<div class="row">
    		<div class="col-md-12" align="center">
    			<h3 class="section-title">LOG <?php echo $_SESSION["rol"]?></h3>
    			<div class="section-title-divider"></div>
    		</div>	
		</div>
		
		<br>
		<div class="row">
    		<div class="col-md-12" align="center">
    			<div class="card3">
        			<div class="row">
        				<div class="col-md-3" align="right">
							<div class="card-header">								
								<font face="Arial Black" size="5">
									<i class="fas fa-search"></i> Buscar
								</font>							
							</div>
						</div>
						
						<div class="col-md-9" align="left">
        					<div class="card-header">
								<div class="form-group">
									<input type="text" id="filtro" class="form-control"
										placeholder="Filtro">
								</div>
							</div>
        				</div>					
    				</div>			
    	
    			</div>
    		</div>	
    	</div>
		
		<br><br>

		<div class="row mt-3">
			<div class="col">
				<div id="resultados"></div>
			</div>
		</div>
	
		<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>			
	</div>
</section>

<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>	


<!--Este es el JQuery para buscar LOG: -->
<script>
    $(document).ready(function(){
    	$("#filtro").keyup(function(){
    		if($("#filtro").val().length > 0)
        	{
            	//Encriptamos la ruta para que no sea visible en la inspección de codigo:
    			url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/buscarLogAjax.php") ?>&filtro=" + $("#filtro").val();
    			$("#resultados").load(url);
    		} 
    	});
    });
</script>
