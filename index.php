<?php 
    //Para recuperar la variable sesion cuando se recarga la pagina:
    session_start();
    //Para importar las clases que necesito usar:
    require "logica/Usuario.php";
    require "logica/Producto.php";
    require "logica/Pedido.php";
    require "logica/Carrito.php";
    require "logica/Log.php";
    require "persistencia/Conexion.php";
    
    //Zona horaria en colombia:
    date_default_timezone_set('America/Bogota'); 
    
    //Arreglo de paginas que no requieren session:
    //$pagSinSession = array("presentacion/recuperarClave.php" , "presentacion/autenticar.php");
    
    //Para cerrar la session:
    if (isset($_GET["session"]) && $_GET["session"] == 0)
    {
        $_SESSION["id"] = null; //Se vacia el objeto de session, para romper esa session.
    }
    
    //Este algoritmo es para que me muestre los errores
    //en tiempo de ejecuci�n:
    //ini_set('display_errors', 1);
    //ini_set('display_startup_errors', 1);
    //error_reporting(E_ALL);
    ////////////////////////////////////////////////////
    
    //Pid es una variable para redireccionar de una a otra pagina:
    $pid=NULL;
    if (isset($_GET["pid"]))
    {
        //En pid se guarda la ruta decodificada / desencriptada:
        $pid = base64_decode($_GET["pid"]);
    }
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>JB Liquors</title>
        <!-- Estas linea son para el resposive = ajustar el contenido al tama�o de la ventana -->
        <meta name="viewport"
        	content="width=device-width, initial-scale=1, shrink-to-fit=no">      	
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">     
        
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">  
        <!-- Esta linea es para implementar: Font Awesome (iconos) -->
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css"/>             
        <!-- Place your favicon.ico and apple-touch-icon.png in the template root directory -->
        		<link href="favicon.ico" rel="shortcut icon">       
        <!-- Google Fonts -->
        		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800"
            			rel="stylesheet">      
        <!-- Bootstrap CSS File -->
        		<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">     
        <!-- Libraries CSS Files -->
                <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
                <link href="lib/animate-css/animate.min.css" rel="stylesheet">  
                
                     
        <!-- Main Stylesheet File -->
            		<link href="css/style.css" rel="stylesheet">    
        
        <!-- Estas lineas son para implementar: jQuery, Popper.js y Bootstrap JS -->
        <script 
        	src="https://code.jquery.com/jquery-3.5.1.min.js">
        </script>
        <script
        	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js">
    	</script>
        <script
        	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js">
    	</script>   	
        <!-- Estas son librerias JavaScript para animaciones -->
        <script src="lib/jquery/jquery.min.js"></script>
        <script src="lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="lib/superfish/hoverIntent.js"></script>
        <script src="lib/superfish/superfish.min.js"></script>
        <script src="lib/morphext/morphext.min.js"></script>
        <script src="lib/wow/wow.min.js"></script>
        <script src="lib/stickyjs/sticky.js"></script>
        <script src="lib/easing/easing.js"></script>     
        <!-- Template Specisifc Custom Javascript File -->
        <script src="js/custom.js"></script>
        
        <link rel="shortcut icon" type="image/png" href="img/logo.png">
        <!-- Estas lineas son para implementar tooltip (Peque�as etiquetas descriptivas de botones) -->
        <script type="text/javascript">
        	$(function () 
        	{
              $('[data-toggle="tooltip"]').tooltip()
            })
    	</script> 	
    	
	</head>   
    <body>  	 	
    	<?php 
    	//Para validar si existe session y si es una pagina que no necesita session:
    	if (isset($pid))
    	{
    	    //Si existe m es porque se dirige para una seccion que 
    	    //requiere alguno de los 4 menus:
    	    if (isset($_GET["m"]))
    	    {
    	        //Si existe una sesion, verifica que menu necesita de los 3 roles:
    	        if (isset($_SESSION["id"]))
    	        {
    	            /*Se verifica que menu es el que necesita:
    	             - menu cliente
    	             - menu admin
    	             - menu proveedor */
    	            
    	            if ($_SESSION["rol"] == "Administrador")
    	            {
    	                include "presentacion/administrador/menuAdministrador.php";
    	                include $pid;
    	            }
    	            else if ($_SESSION["rol"] == "Cliente")
    	            {
    	                include "presentacion/cliente/menuCliente.php";
    	                include $pid;
    	            }
    	            else
    	            {
    	                include "presentacion/proveedor/menuProveedor.php";
    	                include $pid;
    	            }
    	        }
    	        else
    	        {
    	            //Quiere decir que no se ha iniciado sesion:
    	             	            
    	            //S� existe verificar es porque se quiere a�adir un producto al carrito,
    	            //entonces se debe reportar error porque no hay sesion activa:
    	            if (isset($_GET["verificar"]))
    	            {
    	                //Redirecciona a los mismos productos reportando un error:
    	                header ("location: index.php?pid=" . base64_encode("presentacion/productos/consultarProductos.php") . "&m&error=1&categoria=" . $_GET["categoria"] . "");
    	            }
    	            else 
    	            {
    	                //Se dirige a una seccion del menu principal:
    	                include "presentacion/menuPrincipal/menu.php";
    	                include $pid;
    	            }
    	        }  	        	
    	    }
    	    else if (isset($_GET["pre"]))
    	    {
    	        //Si existe pre es porque se dirige para la pre-inicio
    	        //que es la primera vista que muestra al iniciar, entonces
    	        //lo envio al inicio y ademas le envio el encabezado:
    	        include "presentacion/encabezado.php";
    	        include $pid;
    	    }  	  
    	    else
    	    {   
    	        //Todas las paginas que no necesitan ni encabezado, ni menu principal:
    	        include $pid;
    	    }
    	}
    	else
    	{
    	    include "presentacion/encabezado.php";
    	    include "presentacion/inicio.php";
    	}
?>   	
    </body>
</html>
